
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

public class Main extends Application {
    public String projectID;
    public String userID;
    public String password;
    public File dataFile;
    public GridPane gridPane;
    
    String chooseFile = "choose file";


    

    @Override
    public void start(Stage stage) {

        Text text1 = new Text("Project ID");
        Text text2 = new Text("Username");
        Text text3 = new Text("Password");
        //TODO: add drag and drop functionality ?
        Text text5 = new Text("File");

        TextField textField1 = new TextField();
        TextField textField2 = new TextField();
        PasswordField textField3 = new PasswordField();
        
       

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose File");

        //TODO: set on action
        Button button1 = new Button("Submit and Start");
        button1.setOnAction(e -> {
            projectID = textField1.getText();
            userID = textField2.getText();
            //TODO: password correct/incorrect ?
            password = textField3.getText();
            if(allFilled()){
            	runJar();
            	gridPane.getChildren().clear();
            	gridPane.add(new Text("Training started"), 3,3);
            }
            else{
            	gridPane.add(new Text("Please fill out all fields"), 0, 6);
            }
        });

        Button button3 = new Button(chooseFile);
        button3.setOnAction(e -> {
            dataFile = fileChooser.showOpenDialog(stage);
            if(dataFile != null){            	
            	button3.setText(dataFile.getName());
            }
        });
        Button button2 = new Button("Clear");
        button2.setOnAction(e -> {
            textField1.clear();
            textField2.clear();
            textField3.clear();
            dataFile = null;
            button3.setText(chooseFile);
        });


        gridPane = new GridPane();
        gridPane.setMinSize(400, 200);
        gridPane.setPadding(new Insets(10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.CENTER);

        gridPane.add(text1, 0, 0);
        gridPane.add(textField1, 1, 0);
        gridPane.add(text2, 0, 1);
        gridPane.add(textField2, 1, 1);
        gridPane.add(text3, 0, 2);
        gridPane.add(textField3, 1, 2);
        gridPane.add(text5, 0, 4);
        gridPane.add(button3, 1, 4);


        gridPane.add(button1, 0, 5);
        gridPane.add(button2, 1, 5);

        Scene scene = new Scene(gridPane);
        stage.setTitle("Login");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.show();
    }
    
    
    
    public void runJar(){
		try {
			String method = getRegressionType();
			System.out.println(method);
			
			if(method.equals("LI")){
				
			}
			
			if(method.equals("LA")){
				String path = this.dataFile.getAbsolutePath();
				path = path.replace("\\", "/");
				ProcessBuilder pb = new ProcessBuilder("java", "-jar", "Lasso_User.jar", this.projectID, this.userID, this.password, path);
				pb.start();
				System.out.println("started");
				
			}
			
			
			if(method.equals("LO")) {
				String path = this.dataFile.getAbsolutePath();
				path = path.replace("\\", "/");
				ProcessBuilder pb = new ProcessBuilder("java", "-jar", "Logistic_User.jar", this.projectID, this.userID, this.password, path);
				pb.start();
				System.out.println("started");
				
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
    
    
    public String getRegressionType(){
    	try {
			String response = JavaPostRequestJWT.makeGetRequest("http://127.0.0.1:8000/type/" + this.projectID);
			System.out.println(response);
			String type = getMapFromJson(response).get("type");
			return type;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return null;
    }
    
    
    private HashMap<String,String> getMapFromJson(String json){
        HashMap<String,String> map = new HashMap<>();
        json = json.trim();
        json = json.substring(1,json.length()-1);
        json = json.replaceAll("\"","");
        String[] pairs = json.split(",");
        for(String pair: pairs){
            String[] split_pair = pair.split(":");
            map.put(split_pair[0].trim(),split_pair[1].trim());
        }
        return map;
    }
    

    /**
     * returns true if all fields have been filled out
     * @return
     */
    public boolean allFilled(){
    	return (projectID != null && userID != null && password != null && dataFile != null);
    }
    

    public static void main(String args[]) {
        launch(args);
    }
}