
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class JavaPostRequestJWT {

    private static HttpURLConnection con;
    
    
    
    
    /**
     * makes a get request to the specified url and returns the response
     * @param url
     * @return
     * @throws IOException
     */
    public static String makeGetRequest(String url) throws IOException{

        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Accept","application/json");


            return getResponseFromConnection(con);
            
        } finally {
            con.disconnect();
        }
    }

        public static String getResponseFromConnection(HttpURLConnection con) throws IOException{
    	
        StringBuilder content;

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String line;
            content = new StringBuilder();

            while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        }

        
        return content.toString();
    }
    
    
    
}