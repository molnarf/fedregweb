import java.io.*;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.Matrix;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.ReplaceMissingValues;

public class MyUtils {

	
	
	public static String doubleArrayToString(double[] array){
		StringBuilder sb = new StringBuilder();
		
		sb.append("[");
		boolean first = true;
		for(double elem : array){
			if(first){
				sb.append(elem);
				first = false;
			}
			else{
				sb.append(", " + elem);
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	
	public static double[] deleteEntryAt(double[] x, int index){
		
		double[] output = new double[x.length-1];
		for(int i = 0, k = 0; i<x.length; i++){
			if(i==index){
				continue;
			}
			output[k++] = x[i];
		}
		
		return output;
	}
	
	public static String[] deleteEntryAt(String[] x, int index){
		
		String[] output = new String[x.length-1];
		for(int i = 0, k = 0; i<x.length; i++){
			if(i==index){
				continue;
			}
			output[k++] = x[i];
		}
		
		return output;
	}
	
	
	/**
	 * calculates the sigmoid probability,	betas have to be in order with intercept at position 0
	 * sigmoid function :=   1 / (1 + e^-z)		z := b0 + b1*x1 + b2*x2 + ... + bn*xn
	 * @param x
	 * @param betas
	 * @return
	 */
	public static double sigmoid(double[] x, double[] betas){
		
		testForLength(x, betas);
		
		double z = betas[0];
		
		for(int i = 1 ; i<betas.length; i++){
			z += x[i-1] * betas[i];
		}
		
		return 1.0 / (1.0 + Math.exp(-z));
	}
	
	
	/**
	 * 	calculates the log likelihood for a sample
	 * likelihood = y * log(P(y|x[], betas)) + (1-y) * log(1 - P(y|x[], betas))
	 * @param x values for x�s of a sample
	 * @param y class value of the sample
	 * @param betas	(coefficients)
	 * @return
	 */
	public static double log_likelihood(double[] x, double y, double[] betas){
		testForLength(x, betas);
		double sigmoidProb = sigmoid(x, betas);
		
		double log_likelihood = y * Math.log(sigmoidProb) + (1-y) * Math.log(1-sigmoidProb);
		
		return log_likelihood;
	}
	
	/**
	 * calculates the log likelihood of the coefficients given the samples in the dataset
	 * @param dataset
	 * @param betas
	 * @return
	 */
	public static double log_likelihood(Instances dataset, double[] betas, int classIndex){
		
		double ll = 0;
		
		for(Instance instance : dataset){
			//double classValue = getInverseClassValue(instance);	// weka uses the opposite classvalue for some reason
			double classValue = instance.classValue();
			
			// last entry of instance.toDoubleArray is the class value and has to be deleted
			ll += log_likelihood(deleteEntryAt(instance.toDoubleArray(), classIndex), classValue, betas); // log likelihood
		}
		
		return ll;
	}
	
	

	
	
	
	/**
	 * calculates and returns the gradient of a sample
	 * the gradient is a vector containing the partial derivatives of the log likelihood function
	 * so every row is partially derived after one of the betas and the value of the this derivative is calculated using the data of the sample
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix gradient(double[] x, double y, double[] betas){
		
		DenseMatrix gradient = new DenseMatrix(betas.length, 1);
		
		double sigmoidProb = sigmoid(x,betas);
		
		for(int i = 0; i<betas.length - 1; i++){
			gradient.set(i, 0, (y - sigmoidProb) * x[i]);
		}
		
		gradient.set(betas.length - 1, 0, (y-sigmoidProb));	// the intercept has no x, so * x[i] becomes * 1
		
		return gradient;
	}
	
	
	/**
	 * Calculates and returns the gradient of a dataset
	 * The gradient is a vector containing the partial derivatives of the log likelihood function
	 * so every row is the partial derivative of one of the betas 
	 * and the value of the this derivative is calculated using the data of the dataset
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix gradient(Instances dataset, double[] betas, int classIndex){
	
		DenseMatrix gradient = new DenseMatrix(betas.length, 1);
	
		for(Instance instance : dataset){
		
			//double classValue = getInverseClassValue(instance);	// weka uses the opposite classvalue for some reason
			double classValue = instance.classValue();
			
			// last entry of instance.toDoubleArray is the class value and has to be deleted
			DenseMatrix currentGradient = gradient(deleteEntryAt(instance.toDoubleArray(), classIndex), classValue, betas);
			gradient = (DenseMatrix) gradient.add(currentGradient);
		}

		return gradient;
	}
	

	
	
	/**
	 * returns the Hessian matrix of a sample, which is a square matrix of second order partial derivatives
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix hessian(double[] x, int y, double[] betas){
		DenseMatrix hessian = new DenseMatrix(betas.length, betas.length);
		double sigmoidProb = sigmoid(x,betas);
		for(int i = 0; i<betas.length; i++){
			for(int j = 0; j<betas.length; j++){
				double xi;
				double xj;
				if(i == betas.length-1){
					xi = 1;
				}
				else{
					xi = x[i];
				}
				if(j == betas.length-1){
					xj = 1;
				}
				else{
					xj = x[j];
				}
				
				hessian.set(i, j, sigmoidProb * (1 - sigmoidProb) * xi * xj);
			}
		}

		return hessian;
	}
	
	/**
	 * returns the Hessian matrix of a dataset, which is a square matrix of second order partial derivatives
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix hessian(Instances dataset, double[] betas, int classIndex){
		
		DenseMatrix hessian = new DenseMatrix(betas.length, betas.length);
		for(Instance instance : dataset){
			
			// entry of instance.toDoubleArray with the class value, has to be deleted
			DenseMatrix currentHessian = hessian(deleteEntryAt(instance.toDoubleArray(), classIndex), instance.classIndex(), betas);
			hessian = (DenseMatrix) hessian.add(currentHessian);
			
		}
			
		return hessian;
	}
	

	
	
	/**
	 * tests if the number of betas equals the number of x�s + 1
	 * @param x
	 * @param betas
	 */
	public static void testForLength(double[] x, double[]betas){
		if(x.length + 1 != betas.length){
			System.out.println("x length: " + x.length + "    betas length: " + betas.length);
			throw new RuntimeException("Arrays have the wrong length");
		}
	}
	
	
	public static String doubleArrayToString(double[][] array){
		StringBuilder sb = new StringBuilder();
		
		boolean first = true;
		
		for(int i = 0; i<array.length; i++){
			first = true;
			for(int j = 0; j<array[0].length; j++){
				if(first){
					sb.append(array[i][j]);
					first = false;
				}
				else{
					sb.append(", " + array[i][j]);
				}
			}
			
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	
	public static String matrixToString(Matrix x){
		
		double[][] d = matrixToDoubleArray(x);
	
		return doubleArrayToString(d);
	}
	
	
	public static double[][] matrixToDoubleArray(Matrix x){
		
		double[][] d = new double[x.numRows()][x.numColumns()];
			
		for(int i = 0; i<x.numRows(); i++){
			for(int j = 0; j<x.numColumns(); j++){
				d[i][j] = x.get(i, j);
			}
		}
		
		return d;
	}
	
	
	/**
	 * 1 2
	 * 3 4        --> 1, 2\n3, 4
	 * @param x
	 * @return
	 */
	public static String matrixToJson(Matrix x){
		double[][] array = matrixToDoubleArray(x);
		
		StringBuilder sb = new StringBuilder();
		
		boolean first = true;
		
		for(int i = 0; i<array.length; i++){
			first = true;
			for(int j = 0; j<array[0].length; j++){
				if(first){
					sb.append(array[i][j]);
					first = false;
				}
				else{
					sb.append(", " + array[i][j]);
				}
			}
			
			sb.append("\\n");
		}
		
		return sb.toString();
	}
	
	/**
	 * return the inverse of the input matrix
	 * @param A
	 * @return
	 */
	public static DenseMatrix inverseMatrix(DenseMatrix A){
		
		DenseMatrix I = Matrices.identity(A.numRows()); // Identitiy Matrix
		DenseMatrix inverse = new DenseMatrix(A.numRows(), A.numColumns());
		
		A.solve(I, inverse);	// not sure why it is not I.solve(A, inverse)
		
		return inverse;
	}
	

	/**
	 * returns 0 if the classvalue of the instance is 1 and the other way around
	 * @param instance
	 * @return
	 */
	public static double getInverseClassValue(Instance instance){
		double classValue;
		if(instance.classValue() == 1.0){
			classValue = 0;
		}
		else{
			classValue = 1.0;
		}
		return classValue;
	}
	
	
	/**
	 * replaces missing values in the given a dataset
	 * @param dataset
	 */
	public static Instances replaceMissingValues(Instances dataset){
		ReplaceMissingValues m_ReplaceMissingValues = new ReplaceMissingValues();
	    try {
	    	dataset.deleteWithMissingClass();
			m_ReplaceMissingValues.setInputFormat(dataset);
			dataset = Filter.useFilter(dataset, m_ReplaceMissingValues);
		} catch (Exception e) {
			e.printStackTrace();
		}
	    return dataset;
	}
	
	
	
	
	/**
	 * returns true if the numbers in the array have a different value and false if they all have the same
	 * @param numbers
	 * @return
	 */
	public static boolean hasDifferentValues(String[] numbers){
		
		if(numbers.length == 0){
			throw new RuntimeException("array is empty");
		}
		double firstValue = Double.parseDouble(numbers[0]);
		for(int i = 1; i<numbers.length; i++){
			if(firstValue != Double.parseDouble(numbers[i])){
				return true;
			}
		}
		
		
		return false;
	}
	
	
	/**
	 * raw_data should have the format {"regression_model": "3.1, 12.3, 1.5, 7.3"}
	 * returns the model (betas) as double[]
	 * @param raw_data
	 * @return
	 */
	public static double[] cutModelFromRawData(String raw_data){
		String[] fields = raw_data.split("\"");
		String[] numbers = fields[3].split(", ");
		
		double[] betas = new double[numbers.length];
		
		System.out.println("Response:\n" + raw_data);
		
		for(int i = 0; i < numbers.length; i++){
			if(isNumeric(numbers[i])){
				betas[i] = Double.parseDouble(numbers[i]);
			}
			else{
				System.err.println("The model on the server is in the wrong format or empty, so null was returned for the betas!");
				return null;
			}		}
		
		return betas;
	}
	
	
	/**
	 * calculates the correlationCoefficient of two double arrays
	 * @param x
	 * @param y
	 * @return
	 */
	public static double correlationCoefficient(double[] x, double[] y){
		double correlationCoefficient = 0;
		
		double E_x = 0;
		double E_y = 0;
		
		for(int i = 0; i<x.length; i++){
			E_x += x[i];
			E_y += y[i];
		}
		
		E_x /= x.length;
		E_y /= y.length;
		
		double counter = 0;	// Zaehler
		double denominator = 0; // Nenner
		
		double SSDx = 0;	// sum of squared deviations
		double SSDy = 0;	
		
		for(int i = 0; i<x.length; i++){
			counter += (x[i] - E_x) * (y[i] - E_y);
			SSDx += Math.pow(x[i] - E_x, 2);
			SSDy += Math.pow(y[i] - E_y, 2);
		}

		denominator = Math.sqrt(SSDx * SSDy);
		
		correlationCoefficient = counter / denominator;
		
		return correlationCoefficient;
	}
	
	
	
	/**
	 * returns the current model (betas) from the server
	 * @param username
	 * @param password
	 * @param project_url e.g. "http://127.0.0.1:8000/logistic_model/vM5daRgIcEno/"
	 * @return
	 */
	public static double[] getModelFromServer(String username, String password, String project_url){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			
			String raw_data = JavaPostRequestJWT.makeGetRequestWithToken(project_url, token);
			
			double[] betas = cutModelFromRawData(raw_data);
			
			return betas;
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	/**
	 * 
	 * @param username
	 * @param password
	 * @param project_url e.g. http://127.0.0.1:8000/logistic_ll/5zfzzccNbHML/
	 * @return double[2] with double[0] = old_ll and double[1] = new_ll
	 */
	public static double[] getLikelihoodsFromServer(String username, String password, String project_url){
		double[] likelihoods = new double[2];
		
		try {
			String raw_data = JavaPostRequestJWT.makeGetRequest(project_url);
			
			likelihoods = cutLikelihoods(raw_data);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return likelihoods;
	}
	
	
	
	/**
	 * raw_data should have the format {"log_likelihood_old": "3.1", "log_likelihood_new": "2.1"}
	 * returns the absolute difference between the old and new log_likelihood
	 * @param raw_data
	 * @return
	 */
	public static double[] cutLikelihoods(String raw_data){
		String[] fields = raw_data.split("\"");
		
		double[] likelihoods = new double[2];
		
		for(int i = 0; i < fields.length; i++){
			if(fields[i].equals("log_likelihood_old")){
				if(fields[i+2].equals("")){
					// in the first and second iteration there is no value for the old ll, so Double.MAX is returned as difference
					likelihoods[0] = Double.MAX_VALUE;
				}
				else{
					likelihoods[0] = Double.parseDouble(fields[i+2]);
				}
			}
			if(fields[i].equals("log_likelihood_new")){
				if(fields[i+2].equals("")){
					// in the first and second iteration there is no value for the old ll, so Double.MAX is returned as difference
					likelihoods[1] = Double.MAX_VALUE;
				}
				else{
					likelihoods[1] = Double.parseDouble(fields[i+2]);
				}
			}
		}
		
		return likelihoods;
	}
	
	
	/**
	 * returns the absolute difference of both likelihoods
	 * @param old_ll
	 * @param new_ll
	 * @return
	 */
	public static double likelihoodDifference(double old_ll, double new_ll){
		
		if(old_ll == Double.MAX_VALUE || new_ll == Double.MAX_VALUE){
			return Double.MAX_VALUE;
		}
		
		return Math.abs(new_ll - old_ll); 
	}
	
	
	
	
	/**
	 * returns true if the given String is a number
	 * @param strNum
	 * @return
	 */
	public static boolean isNumeric(String strNum) {
	    try {
	        @SuppressWarnings("unused")
			double d = Double.parseDouble(strNum);
	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false;
	    }
	    return true;
	}
}
