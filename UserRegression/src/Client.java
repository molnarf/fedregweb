import java.io.IOException;
import java.util.ArrayList;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.UpperSymmDenseMatrix;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Client {
	
	private Instances dataset;
	private int samplesize;
	private ArrayList<Attribute> attributes;		// this list is only here so the order of attributes is saved
	private int classIndex;
	
	public double[] betas;
	
	private DenseMatrix hessian;
	private DenseMatrix gradient;
	private double log_likelihood;
	
	private String username;
	private String password;
	private String project_id;
	
	
	/**
	 * creates a new Client 
	 * @param arffPath path to the data file
	 * @param classIndex (defaults to attribute.size() -1)
	 * @param username
	 * @param password
	 * @param project_id
	 */
	public Client(String arffPath, int classIndex, String username, String password, String project_id){
		this.attributes = new ArrayList<>();
		this.classIndex = classIndex;
		this.username = username;
		this.password = password;
		this.project_id = project_id;
		DataSource source;
		try {
			source = new DataSource(arffPath);
			this.dataset = source.getDataSet();
			dataset.setClassIndex(classIndex);
			dataset = MyUtils.replaceMissingValues(dataset);
			saveAttributesAndSamplesize();
			
			if(dataset.get(0).attribute(classIndex).isNumeric()){
				throw new RuntimeException("Cannot handle numeric class!");
			}
			
			this.betas = new double[attributes.size()];
			
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}
	

	
	/**
	 * starts the federated model training 
	 */
	public void start(){
		String project_ll_url = "http://127.0.0.1:8000/logistic_ll/" + project_id  + "/";
		
			double[] lls_old_new = MyUtils.getLikelihoodsFromServer(username, password, project_ll_url);
	
	//		getModelFromServer();	// might be unneccessary
			
			
			while(MyUtils.likelihoodDifference(lls_old_new[0], lls_old_new[1]) > 0.00000000001){
				calculateVariables();
				postDataAndGetNewModelFromServer();
				lls_old_new = MyUtils.getLikelihoodsFromServer(username, password, project_ll_url);
			}
			
			postCovMatrixToServer();
			
			postDataAndGetNewModelFromServer();  // one last time so the server calculates the p-values

	}
	
	
	/**
	 * saves the attributes as list and sets the samplesize
	 */
	private void saveAttributesAndSamplesize(){
		for(int i = 0; i<dataset.numAttributes(); i++){
			this.attributes.add(dataset.attribute(i));
		}
		this.samplesize = dataset.numInstances();
	}
	
	
	
	/**
	 * calculates the hessian, gradient and log_likelihood of the dataset given the current betas
	 */
	public void calculateVariables(){
		hessian();
		gradient();
		log_likelihood();
	}
	
	
	
	/**
	 * return the log likelihood of the clients dataset given the betas
	 * @param betas
	 * @return
	 */
	public double log_likelihood(){
		this.log_likelihood = MyUtils.log_likelihood(dataset, betas, classIndex);
		
		return log_likelihood;
	}
	
	/**
	 * Calculates and returns the gradient of the clients dataset
	 * The gradient is a vector containing the partial derivatives of the log likelihood function
	 * so every row is the partial derivative of one of the betas
	 * and the value of the this derivative is calculated using the data of the dataset
	 * @param betas
	 * @return
	 */
	public DenseMatrix gradient(){
		this.gradient = MyUtils.gradient(dataset, betas, classIndex);
		
		return gradient;
	}
	
	/**
	 * returns the Hessian matrix of the clients dataset, which is a square matrix of second order partial derivatives
	 * of the log likelihood function
	 * @param betas
	 * @return
	 */
	public DenseMatrix hessian(){
		this.hessian = MyUtils.hessian(dataset, betas, classIndex);
		
		return hessian;
	}
	

	/**
	 * calculates the standard error for the given betas
	 * @return standard errors of the coefficients
	 */
	public double[] getStandardErrors(){

		double[] standardErrors = new double[betas.length];
		
		// Establish required matrices 
		DenseMatrix X = new DenseMatrix(samplesize, this.attributes.size());
		UpperSymmDenseMatrix V = new UpperSymmDenseMatrix(samplesize);
		for (int i = 0; i < samplesize; i++) { 
		  double p = MyUtils.sigmoid(MyUtils.deleteEntryAt(dataset.instance(i).toDoubleArray(), classIndex), betas);
		  V.set(i, i, p * (1 - p));
		  int index = 0; 
		  X.set(i, index++, 1.0);
		  for (int j = 0; j < attributes.size(); j++) { 
		    if (j != dataset.classIndex()) { 
		      X.set(i, index++, dataset.instance(i).value(j));
		    } 
		  } 
		} 

		// Compute M = X'VX 
		UpperSymmDenseMatrix M = (UpperSymmDenseMatrix) X.transpose(new DenseMatrix(attributes.size(), samplesize)).mult(V, new DenseMatrix(new DenseMatrix(attributes.size(), samplesize))).mult(X, new UpperSymmDenseMatrix(attributes.size()));

		// Compute covariance matrix for parameters (inverse of M) 
		DenseMatrix I = Matrices.identity(attributes.size()); 
		DenseMatrix C = I.copy();
		C = (DenseMatrix) M.solve(I, C);
		for (int j = 0; j < attributes.size(); j++) { 
		  standardErrors[j] = Math.sqrt(C.get(j, j));	// standard error
		}
		
		return standardErrors;
	}
	
	
	
	/**
	 * returns the covariance of the dataset
	 * @return
	 */
	public UpperSymmDenseMatrix getCovarianceMatrix(){
		
		// Establish required matrices 
		DenseMatrix X = new DenseMatrix(samplesize, this.attributes.size());
		UpperSymmDenseMatrix V = new UpperSymmDenseMatrix(samplesize);
		for (int i = 0; i < samplesize; i++) { 
		  double p = MyUtils.sigmoid(MyUtils.deleteEntryAt(dataset.instance(i).toDoubleArray(), classIndex), betas);
		  V.set(i, i, p * (1 - p));
		  int index = 0; 
		  X.set(i, index++, 1.0);
		  for (int j = 0; j < attributes.size(); j++) { 
		    if (j != dataset.classIndex()) { 
		      X.set(i, index++, dataset.instance(i).value(j));
		    } 
		  } 
		} 

		// Compute M = X'VX 
		UpperSymmDenseMatrix M = (UpperSymmDenseMatrix) X.transpose(new DenseMatrix(attributes.size(), samplesize)).mult(V, new DenseMatrix(new DenseMatrix(attributes.size(), samplesize))).mult(X, new UpperSymmDenseMatrix(attributes.size()));
			
		return M;
	}
	
	
	/**
	 * url e.g. "http://127.0.0.1:8000/logistic/vM5daRgIcEno/"
	 * @return
	 */
	public double[] postDataAndGetNewModelFromServer(){
		try {
			System.out.println("posted data");
			String raw_data = JavaPostRequestJWT.makePostRequest("http://127.0.0.1:8000/logistic/" + project_id + "/", dataToJson());
			System.out.println("got new model");
			
			this.betas = MyUtils.cutModelFromRawData(raw_data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	return betas;
	}
	
	
	/**
	 * posts the Covariance matrix to the server
	 */
	public void postCovMatrixToServer(){
		try {
			String content = "{\"covMatrix\" : \"" + MyUtils.matrixToJson(getCovarianceMatrix()) + "\"}";
			
			String raw_data = JavaPostRequestJWT.makePostRequest("http://127.0.0.1:8000/logistic_covMatrix/" + project_id + "/", content);
			
			System.out.println(raw_data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	/**
	 * get the model from the server and saves it as instance variable
	 */
	public void getModelFromServer(){
		
		double[] model = MyUtils.getModelFromServer(username, password, "http://127.0.0.1:8000/logistic_model/" + this.project_id + "/");
		
		if(model != null){	// if this is the first iteration
			this.betas = model;
		}
		
	}
	
	
	/**
	 * transforms all the data needed for one iteration step into json format
	 * (inludes the hessian, gradient, log_likelihood, attribute names, username and password)
	 * @return
	 */
	public String dataToJson(){
		String JsonOutput = "{\"gradient\" : \"" + MyUtils.matrixToJson(gradient) + "\", "
				   + "\"hessian\" : \"" + MyUtils.matrixToJson(hessian) + "\", "
				   + "\"log_likelihood\" : \"" + this.log_likelihood + "\", "
				   + "\"attribute_names\" : \"" + getAttributeNames() + "\", "
				   + "\"username\" : \"" + this.username + "\", "
				   + "\"password\" : \"" + this.password + "\"}";
		
		return JsonOutput;
	}
	

	
	/**
	 * returns the number of attributes
	 * @return
	 */
	public int getNumAttributes(){
		return this.attributes.size();
	}
	
	
	/**
	 * returns the attribute names of the dataset as string
	 * @return
	 */
	public String getAttributeNames(){
		StringBuilder sb = new StringBuilder();
		
		String prefix = "";
		for(Attribute attribute : this.attributes){
			sb.append(prefix);
			prefix = ", ";
			sb.append(attribute.name());
		}
		
		return sb.toString();
	}
	
	
	public ArrayList<Attribute> getAttributes(){
		return this.attributes;
	}
	
	public int getClassIndex(){
		return this.classIndex;
	}
}
