
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class JavaPostRequestJWT {

    private static HttpURLConnection con;
    
    
    /**
     * makes a post request to the url, containing the url parameters as data
     * @param url
     * @param urlParameters
     * @return
     * @throws IOException
     */
    public static String makePostRequest(String url, String urlParameters) throws IOException{
    	
        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");


            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }


           return getResponseFromConnection(con);


        } finally {
            
            con.disconnect();
        }
    }
    
    /**
     * makes a post request to the url, containing the url parameters as data, using a jwt token
     * @param url
     * @param token
     * @param urlParameters
     * @return
     * @throws IOException
     */
    public static String makePostRequestWithToken(String url, String token, String urlParameters) throws IOException{

        byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setDoOutput(true);
            con.setRequestMethod("POST");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Content-Type", "application/json; utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setRequestProperty("authorization", "Bearer " + token);


            try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
                wr.write(postData);
            }


           return getResponseFromConnection(con);


        } finally {
            
            con.disconnect();
        }
    }
    
    
    /**
     * makes a get request to the specified url and returns the response
     * @param url
     * @return
     * @throws IOException
     */
    public static String makeGetRequest(String url) throws IOException{

        try {

            URL myurl = new URL(url);
            con = (HttpURLConnection) myurl.openConnection();

            con.setRequestMethod("GET");
            con.setRequestProperty("User-Agent", "Java client");
            con.setRequestProperty("Accept","application/json");


            return getResponseFromConnection(con);
            
        } finally {
            con.disconnect();
        }
    }
    
    
    /**
     * makes a get request to the url, using a jwt token
     * @param url
     * @param token
     * @return
     * @throws IOException
     */
    public static String makeGetRequestWithToken(String url, String token)throws IOException{

          try {

              URL myurl = new URL(url);
              con = (HttpURLConnection) myurl.openConnection();

              con.setRequestMethod("GET");
              con.setRequestProperty("User-Agent", "Java client");
              con.setRequestProperty("authorization", "Bearer " + token);
              con.setRequestProperty("Accept","application/json");


              return getResponseFromConnection(con);
              
          } finally {
              con.disconnect();
          }
      
    }
    
    
    /**
     * generates a jwt token from the specified url, using the username and passord as authentication
     * @param url
     * @param username
     * @param password
     * @return
     * @throws IOException
     */
    public static String getToken(String url, String username, String password) throws IOException{

       String urlParameters = "username=" + username + "&password=" + password;

       byte[] postData = urlParameters.getBytes(StandardCharsets.UTF_8);

       try {

           URL myurl = new URL(url);
           con = (HttpURLConnection) myurl.openConnection();

           con.setDoOutput(true);
           con.setRequestMethod("POST");
           con.setRequestProperty("User-Agent", "Java client");
           con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
           con.setRequestProperty("Accept","application/json");

           try (DataOutputStream wr = new DataOutputStream(con.getOutputStream())) {
               wr.write(postData);
           }

           return getResponseFromConnection(con);
           
       } finally {
           con.disconnect();
       }
   
    }

    
    /**
     * cuts out the access token out of the response String obtained from the authorization
     * @param tokenResponse
     * @return
     */
    public static String cutOutToken(String tokenResponse){
    	
    	String[] fields = tokenResponse.split("\"access\"");
    	
    	String token = fields[1];
    	token = token.substring(2, token.length()-4);
    	
    	return token;
    }
    
    
    /**
     * returns the response of an HttpURLConnecition
     * @param con
     * @return
     * @throws IOException
     */
    public static String getResponseFromConnection(HttpURLConnection con) throws IOException{
    	
        StringBuilder content;

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()))) {
            String line;
            content = new StringBuilder();

            while ((line = in.readLine()) != null) {
                content.append(line);
                content.append(System.lineSeparator());
            }
        }

        
        return content.toString();
    }
    
    
    
}