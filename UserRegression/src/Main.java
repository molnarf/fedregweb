import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

public class Main {

	
	public static void main(String[] args){
		
		
		String project_id = args[0];
		String username = args[1];
		String password = args[2];
		String file_path = args[3];
		
		try {
//			String geArff = "C:/Users/Florian/Documents/Vorlesungen_6/Praktische_Arbeit/Testing/diabetes.arff";

				
			DataSource source = new DataSource(file_path);
			Instances dataset = source.getDataSet();
			int classIndex = dataset.numAttributes()-1;
			dataset.setClassIndex(classIndex);

			
			
			
			Client c = new Client(file_path, classIndex, username, password, project_id);
			c.start();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		
	}
}
