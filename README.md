#### FedRegWeb is a website designed to create, manage and participate in federated regression projects.
#### The website includes 3 types of regression: Linear Regression (by Issra Njah), Logistic Regression (by Florian Molnar) and Lasso Regression (by Johannes Kersting)   <br />  <br />

<img src="images/Screenshot.JPG" width="700">
<img src="images/result.JPG" width="400">

Clone the repository with the command "git clone https://gitlab.com/molnarf/fedregweb.git"

Make sure the following libraries are installed:

    Django: "pip install django"
    
    Django REST framework: "pip install djangorestframework"
    
    Django REST framework simple jwt: "pip install djangorestframework_simplejwt"
    

To run the server, head into the "FedRegWeb" directory and type "python manage.py runserver" into the command line.

The server can then be accessed in your browser under "http://127.0.0.1:8000/".

Please note that linear regression is not available in this version
