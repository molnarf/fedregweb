import no.uib.cipr.matrix.DenseMatrix;

public class UserData {
	
	private DenseMatrix gradient;
	private DenseMatrix hessian;
	private double log_likelihood;
	
	public UserData(DenseMatrix gradient, DenseMatrix hessian, double log_likelihood) {
		this.gradient = gradient;
		this.hessian = hessian;
		this.log_likelihood = log_likelihood;
	}

	
	
	public DenseMatrix getGradient() {
		return gradient;
	}

	public DenseMatrix getHessian() {
		return hessian;
	}

	public double getLog_likelihood() {
		return log_likelihood;
	}

}
