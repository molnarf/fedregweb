import java.io.IOException;
import java.util.ArrayList;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.UpperSymmDenseMatrix;

public class ApiUtils {
	
	
	
	/**
	 * posts the betas to the server and updates the server model
	 * @param username
	 * @param password
	 * @param project_url e.g. "http://127.0.0.1:8000/logistic_model/vM5daRgIcEno/"
	 * @param betas
	 */
	public static void postModelToServer(String username, String password, String project_url, double[] betas){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			JavaPostRequestJWT.makePostRequestWithToken(project_url, token, arrayToJson("regression_model", betas));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	/**
	 * posts the p-values, the z-values and the standard errors to the server
	 * @param username
	 * @param password
	 * @param project_url
	 * @param p_values
	 * @param z_values
	 * @param std_errs
	 */
	public static void postPvaluesToServer(String username, String password, String project_url,
			double[] p_values, double[] z_values, double[] std_errs){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			String postData = stringsToJson("p_values", arrayToString(p_values), "z_values", arrayToString(z_values), "std_errs", arrayToString(std_errs));
			JavaPostRequestJWT.makePostRequestWithToken(project_url, token, postData);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/*
	public static void oldpostPvaluesToServer(String username, String password, String project_url,
			double[] p_values){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			JavaPostRequestJWT.makePostRequestWithToken(project_url, token, arrayToJson("p_values", p_values));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	*/
	
	

	
	/**
	 * returns the current model (betas) from the server
	 * @param username
	 * @param password
	 * @param project_url e.g. "http://127.0.0.1:8000/logistic_model/vM5daRgIcEno/"
	 * @return
	 */
	public static double[] getModelFromServer(String username, String password, String project_url){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			
			String raw_data = JavaPostRequestJWT.makeGetRequestWithToken(project_url, token);
			
			double[] betas = cutModelFromRawData(raw_data);
			
			return betas;
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}


	
	
	
/**
 * returns the covariance matrices of the users (used to calculate the p-values of the coefficients)
 * @param username
 * @param password
 * @param project_url e.g. http://127.0.0.1:8000/logistic_covMatrix/3IQtL7TJkkSC/
 * @return
 */
	public static ArrayList<UpperSymmDenseMatrix> getCovMatFromServer(String username, String password, String project_url){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			
			String raw_data = JavaPostRequestJWT.makeGetRequestWithToken(project_url, token);
			
			return cutCovMatFromRawData(raw_data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	

	
	/**
	 * 
	 * @param username
	 * @param password
	 * @param project_url e.g. "http://127.0.0.1:8000/logistic_data/vM5daRgIcEno/"
	 * @return
	 */
	public static ArrayList<UserData> getUserDataFromServer(String username, String password, String project_url){
		
		ArrayList<UserData> userData = new ArrayList<>();
		
		String token;
		try {
			token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			
			String raw_data = JavaPostRequestJWT.makeGetRequestWithToken(project_url, token);
			
			userData = cutUserDataFromRawData(raw_data);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return userData;
	}

	

	

	/**
	 * posts the new likelihoods to the server
	 * @param username
	 * @param password
	 * @param project_url e.g.  http://127.0.0.1:8000/logistic_ll/5zfzzccNbHML/
	 * @param betas
	 */
	public static void postLikelihoodsToServer(String username, String password, String project_url, double old_ll, double new_ll){
		try {
			String token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);

			String jsonData = "{\"log_likelihood_old\" : \"" + old_ll + "\", "
					   + "\"log_likelihood_new\" : \"" + new_ll + "\", "
					   + "\"username\" : \"" + username + "\", "
					   + "\"password\" : \"" + password + "\"}";
			
			JavaPostRequestJWT.makePostRequestWithToken(project_url, token, jsonData);
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	


	
	/**
	 * 
	 * @param username
	 * @param password
	 * @param project_url e.g. http://127.0.0.1:8000/logistic_ll/5zfzzccNbHML/
	 * @return double[2] with double[0] = old_ll and double[1] = new_ll
	 */
	public static double[] getLikelihoodsFromServer(String username, String password, String project_url){
		double[] likelihoods = new double[2];
		
		String token;
		try {
			token = JavaPostRequestJWT.getToken("http://127.0.0.1:8000/api/token/", username, password);
			token = JavaPostRequestJWT.cutOutToken(token);
			
			String raw_data = JavaPostRequestJWT.makeGetRequestWithToken(project_url, token);
			
			likelihoods = cutLikelihoods(raw_data);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return likelihoods;
	}
	
	
	
	
	
	
	
	/**
	 * raw_data should have the format {"log_likelihood_old": "3.1", "log_likelihood_new": "2.1"}
	 * returns the absolute difference between the old and new log_likelihood
	 * @param raw_data
	 * @return
	 */
	public static double[] cutLikelihoods(String raw_data){
		String[] fields = raw_data.split("\"");
		
		double[] likelihoods = new double[2];
		
		for(int i = 0; i < fields.length; i++){
			if(fields[i].equals("log_likelihood_old")){
				if(fields[i+2].equals("")){
					// in the first and second iteration there is no value for the old ll, so Double.MAX is returned as difference
					likelihoods[0] = Double.MAX_VALUE;
				}
				else{
					likelihoods[0] = Double.parseDouble(fields[i+2]);
				}
			}
			if(fields[i].equals("log_likelihood_new")){
				if(fields[i+2].equals("")){
					// in the first and second iteration there is no value for the old ll, so Double.MAX is returned as difference
					likelihoods[1] = Double.MAX_VALUE;
				}
				else{
					likelihoods[1] = Double.parseDouble(fields[i+2]);
				}
			}
		}
		
		return likelihoods;
	}
	
	

	

	
	
	/**
	 * raw_data should have the json format {"covMatrix":"5015.75, 91924.75\n91924.75, 3002189.75\n"}
	 * @param raw_data
	 * @return
	 */
	public static ArrayList<UpperSymmDenseMatrix> cutCovMatFromRawData(String raw_data){
		ArrayList<UpperSymmDenseMatrix> covMatrices = new ArrayList<>();
		
		
		String[] splitData = raw_data.split("\\},\\{"); // data of different users is separated by },{
		
		for(String data : splitData){
			UpperSymmDenseMatrix covMatrix = null;
		
			String[] fields = data.split("\"");
			
			for(int i = 0; i<fields.length; i++){
				if(fields[i].equals("covMatrix")){
					String covMatrixString = fields[i+2];
					DenseMatrix matrix = jsonToMatrix(covMatrixString);
					covMatrix = new UpperSymmDenseMatrix(matrix);
				}
			}
			
			covMatrices.add(covMatrix);
		
		}
		
		return covMatrices;
	}
	
	
	
	
	
	/**
	 * 
	 * raw_data should have the json format [{"hessian":"5015.75, 91924.75\n91924.75, 3002189.75\n","gradient":"1476.5\n46423.5\n","log_likelihood":"-532.3370346700376"},{"hessian":"5015.75, 91924.75\n91924.75, 3002189.75\n","gradient":"1476.5\n46423.5\n","log_likelihood":"-532.3370346700376"}]
	 * returns the model (betas) as double[]
	 * @param raw_data
	 * @return
	 */
	public static ArrayList<UserData> cutUserDataFromRawData(String raw_data){
		ArrayList<UserData> userDatas = new ArrayList<>();

		String[] splitUsers = raw_data.split("\\},\\{"); // data of different users is separated by },{
		
		for(String user : splitUsers){
			DenseMatrix hessian = null;
			DenseMatrix gradient = null;
			double log_likelihood = 0;
			
			String[] fields = user.split("\"");
			
			
			for(int i = 0; i<fields.length; i++){
				if(fields[i].equals("hessian")){
					String hessian_string = fields[i+2];
					hessian = jsonToMatrix(hessian_string);
				}
				
				if(fields[i].equals("gradient")){
					String gradient_string = fields[i+2];
					gradient = jsonToMatrix(gradient_string);
				}
				
				if(fields[i].equals("log_likelihood")){
					log_likelihood = Double.parseDouble(fields[i+2]);
				}
				
			}
			UserData data = new UserData(gradient, hessian, log_likelihood);
			userDatas.add(data);
		}
		
		return userDatas;
	}
	
	
	
	/**
	 * raw_data should have the format {"regression_model": "3.1, 12.3, 1.5, 7.3"}
	 * returns the model (betas) as double[] or null if the parsing was unsuccessful
	 * @param raw_data
	 * @return
	 */
	public static double[] cutModelFromRawData(String raw_data){
		String[] fields = raw_data.split("\"");
		String[] numbers = fields[3].split(", ");
		
		double[] betas = new double[numbers.length];
		
		for(int i = 0; i < numbers.length; i++){
			if(isNumeric(numbers[i])){
				betas[i] = Double.parseDouble(numbers[i]);
			}
			else{
				System.err.println("The model on the server is in the wrong format or empty so null was returned for the betas!");
				return null;
			}
		}
		
		return betas;
	}
	
	


	
	
	/**
	 * example json matrix   5015.75, 91924.75\n91924.75, 3002189.75\n
	 * @param json
	 * @return
	 */
	public static DenseMatrix jsonToMatrix(String json){
		
		String[] rows = json.split("\\\\n");
		int numCols = rows[0].split(", ").length;
		
		double[][] array = new double[rows.length][numCols];
		
		
		for(int i = 0; i<rows.length; i++){
			String[] numbers = rows[i].split(", ");
			for(int j = 0; j<numbers.length; j++){
				array[i][j] = Double.parseDouble(numbers[j]);
			}
		}
		
		DenseMatrix matrix = new DenseMatrix(array);
		
		return matrix;
	}
	
	
	
	/**
	 * takes strings and brings them into json format 
	 * e.g. input: name1, value1, name2, value2
	 * output: "{"name1" : "value1", "name2" : "value2"}"
	 * @param strings
	 * @return
	 */
	public static String stringsToJson(String... strings){
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		
		String prefix = "";
		for(int i = 0; i<strings.length; i+=2){
			sb.append(prefix);
			prefix = "\", ";
			sb.append("\"" + strings[i] +  "\" : \"" + strings[i+1]);
		}
		
		sb.append("\"}");
		
		return sb.toString();
	}
	
	
	/**
	 * 
	 * @param name of the array e.g. regression_model in {"regression_model": "12.4, 3.1, 1.3, 6.4"}
	 * @param array
	 * @return
	 */
	public static String arrayToString(double[] array){
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i<array.length-1; i++){
			sb.append(array[i] + ", ");
		}
		sb.append(array[array.length-1]);
		
		return sb.toString();
	}
	
	
	/**
	 * 
	 * @param name of the array e.g. regression_model in {"regression_model": "12.4, 3.1, 1.3, 6.4"}
	 * @param array
	 * @return
	 */
	public static String arrayToJson(String name, double[] array){
		StringBuilder sb = new StringBuilder();
		sb.append("{\"" + name + "\" : \"");
		
		for(int i = 0; i<array.length-1; i++){
			sb.append(array[i] + ", ");
		}
		sb.append(array[array.length-1]);
		
		sb.append("\"}");
		
		return sb.toString();
	}
	

	
	
	public static boolean isNumeric(String strNum) {
	    try {
	        @SuppressWarnings("unused")
			double d = Double.parseDouble(strNum);
	    } catch (NumberFormatException | NullPointerException nfe) {
	        return false;
	    }
	    return true;
	}
	
	
	
}

