import weka.core.Attribute;

public class LRattribute implements Comparable<LRattribute>{

	private Attribute attribute;
	private double pValue;
	private double standard_error;
	private double z_value;
	private double estimate;
	
	/**
	 * 
	 * @param attribute
	 * @param estimate
	 * @param standard_error
	 * @param zValue
	 * @param pValue
	 */
	public LRattribute(Attribute attribute,double estimate, double standard_error, double zValue, double pValue){
		this.attribute = attribute;
		this.pValue = pValue;
		this.standard_error = standard_error;
		this.z_value = zValue;
		this.estimate = estimate;
	}

	
	
	public Attribute getAttribute(){
		return this.attribute;
	}



	@Override
	public int compareTo(LRattribute other) {

		if (this.pValue < other.pValue) {
			return -1;
		}
		else if(this.pValue > other.pValue){
			return 1;
		}
		else{
			return 0;
		}
	
	}
	
	public String toString(){
		return attribute.name() + "\testimate: " + estimate + "\tStd. Error: " + standard_error + "\tz-value: " + z_value + "\tp-value: " + pValue;
	}
}

