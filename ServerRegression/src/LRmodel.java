import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import weka.classifiers.Evaluation;
import weka.classifiers.functions.Logistic;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;
import weka.core.Utils;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.UpperSymmDenseMatrix;
import no.uib.cipr.matrix.Matrices;



public class LRmodel extends Logistic{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 786189571269712387L;
	private int numAttributes;
	public double[] betas;
	public double[] p_values;
	public double[] z_values;
	public double[] std_errs;
	
	private int classIndex;
	public HashMap<String, Double> AttrToPval;
	
	private ArrayList<String> attribute_names;
//	private ArrayList<Attribute> attributes;		// this list is only here so the order of attributes is saved
//	private ArrayList<LRattribute> LRattributes;
	
	private ArrayList<DenseMatrix> gradients;
	private ArrayList<DenseMatrix> hessians;
	
	private ArrayList<UpperSymmDenseMatrix> cov_matrices;	// covariance matrices
	

	
	/**
	 * 
	 * @param gradients of every User
	 * @param hessians of every User
	 * @param classIndex is the index of the parameter that should be predicted
	 */
	public LRmodel(ArrayList<DenseMatrix> gradients, ArrayList<DenseMatrix> hessians, int classIndex, double[] currentBetas){
		this.gradients = gradients;
		this.hessians = hessians;
		this.betas = currentBetas;
		
		if(! MyUtils.matricesHaveSameDimensions(gradients) || ! MyUtils.matricesHaveSameDimensions(hessians)){
			throw new RuntimeException("Matrices do not have the same dimensions");
		}
		if(betas == null || betas.length == 0){	// when there is no model yet (in the first iteration)
			betas = new double[hessians.get(0).numColumns()]; 
		}
		
		this.numAttributes = hessians.get(0).numColumns();	// the number of attributes is the same as the #cols or #rows of the hessian 
	}
	
	
	// constructor for the p-value calculation
	public LRmodel(ArrayList<UpperSymmDenseMatrix> cov_matrices, double[] currentBetas, int classIndex){
		this.cov_matrices = cov_matrices;
		this.betas = currentBetas;
		this.numAttributes = cov_matrices.get(0).numColumns();
		this.classIndex = classIndex;
	}
	
	
	
	/**
	 * performs one iteration to calculate the coefficients (betas) using Newton�s method
	 * and saves the betas
	 */
	public void federatedCoefficientCalculation(){
		
		
		// the new betas are calculated using the Newton�s method
		// betas_new = betas_old + hessian^-1(ll) * gradient(ll)
		
		DenseMatrix gradient = MyUtils.gradient(gradients);
		DenseMatrix hessian = MyUtils.hessian(hessians);
		DenseMatrix inverseHessian = MyUtils.inverseMatrix(hessian);
		
		DenseMatrix betaDifferences = new DenseMatrix(gradient.numRows(), 1);
		
		inverseHessian.mult(gradient, betaDifferences);
		
		for(int k = 1; k<gradient.numRows(); k++){
			betas[k] += betaDifferences.get(k-1, 0);
		}
		betas[0] += betaDifferences.get(gradient.numRows()-1, 0);	// beta zero has index 0 in betas[] or coefficients,
																	// but is the last element in the gradient

			

		setCoefficients(new double[][]{betas});
	}
	
		
	
	/**
	 * calculates the p-values of the model using the covariance matrices of the users
	 * and saves them 
	 */
	public void calculateFederatedPvalues(){
		this.AttrToPval = new HashMap<>();
//		this.LRattributes = new ArrayList<>();
		
		p_values = new double[numAttributes];
		z_values = new double[numAttributes];
		std_errs = new double[numAttributes];
		
		UpperSymmDenseMatrix M = MyUtils.getCovarianceMatrix(cov_matrices);
		
		// Compute covariance matrix C for parameters (inverse of M) 
		DenseMatrix I = Matrices.identity(this.numAttributes); 
		DenseMatrix C = I.copy();
		C = (DenseMatrix) M.solve(I, C);
		
//		System.out.println("\tEstimate\tStd. Error\tz value\tPr(>|z|)"); 

		for (int j = 0, k = 0; j < numAttributes; j++, k++) {
			if (j == 0) {
				System.out.print("Interc.");
			}
			else {
//				System.out.print(this.attribute_names.get(k - 1));
			}
			if (j == this.classIndex) {
				k++;
			}
			double c = this.betas[j];
			double e = Math.sqrt(C.get(j, j)); // standard error
			double z = c / e; // z value = (beta value - expected beta value) /
								// standard deviation
								// entspricht dem zentralen Grenzwertsatz

			double p = 2.0 * (1.0 - weka.core.Statistics.normalProbability(Math.abs(z)));

			System.out.print("\t" + Utils.doubleToString(c, 7));
			System.out.print("\t" + Utils.doubleToString(e, 7));
			System.out.print("\t" + Utils.doubleToString(z, 3));
			System.out.println("\t" + Utils.doubleToString(p, 6));

			p_values[j] = p;
			z_values[j] = z;
			std_errs[j] = e;
			
//			Attribute attribute;
//			if (j == 0) {
//				this.AttrToPval.put("Intercept", p);
//				attribute = new Attribute("intercept");
//			}
//			else{
//				this.AttrToPval.put(this.attribute_names.get(k - 1), p);
//				attribute = clients.get(0).getAttributes().get(k-1);
//			}
//			this.LRattributes.add(new LRattribute(attribute, c, e, z, p));
		
		}
		
		
	}
	
	
	
	public void evaluate(Instances dataset){
		try {
			
			Evaluation eval = new Evaluation(dataset);
			eval.crossValidateModel(this, dataset, 10, new Random(1));
			System.out.println(eval.toSummaryString());

		} catch (Exception e) {
			e.printStackTrace();
		}

		
	}
	
	public double[][] getCoefficients(){
		return this.coefficients().clone();
	}
	
	public void setCoefficients(double[][] coefficients){
		this.m_Par = coefficients;
	}
	
	public void setCoefficients(double[] coefficients){
		double[][] temp = new double[coefficients.length][1];
		for(int i = 0; i<coefficients.length; i++){
			temp[i][0] = coefficients[i];
		}
		setCoefficients(temp);
	}
	
	public double[] getBetas(){					// for whatever reason the coefficients function return a 2 dimensional array 
												// where one length is 1
		double[][] betas = this.coefficients();
		
		double[] output = new double[betas.length];
		
		for(int i = 0; i<betas.length; i++){
			output[i] = betas[i][0];
		}
		
		return output;
	}


	public void myEvaluation(Instances dataset){
		int right = 0;
		int wrong = 0;
		for(Instance i : dataset){
			double p = MyUtils.sigmoid(MyUtils.deleteEntryAt(i.toDoubleArray(), classIndex), this.betas);
			double prediction;
			if(p > 0.5){
				prediction = 0;
			}
			else{
				prediction = 1;
			}
			
			if(prediction == i.classValue()){
				right++;
			}
			else{
				wrong++;
			}
		}
		
		System.out.println("Evaluation : \n" + "Right : " + right + " (" + ((double)right/(right+wrong)) + ")\nWrong : " + wrong + " (" + ((double)wrong/(right+wrong)) + ")");
	}

	
	
	/**
	 * returns the probability for each instance in an array
	 * @param dataset
	 */
	public double[] myEvaluationProbabilities(Instances dataset){
		double[] probabilities = new double[dataset.size()];
		
		for(int i = 0; i<probabilities.length; i++){
			double p = MyUtils.sigmoid(MyUtils.deleteEntryAt(dataset.get(i).toDoubleArray(), classIndex), this.betas);
			probabilities[i] = p;
			
		}
		
		return probabilities;
	}

	
	
	
	
	

}
