import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.Matrices;
import no.uib.cipr.matrix.Matrix;
import no.uib.cipr.matrix.UpperSymmDenseMatrix;
import weka.core.Instance;

public class MyUtils {

	
	public static String doubleArrayToString(double[] array){
		StringBuilder sb = new StringBuilder();
		
		sb.append("[");
		boolean first = true;
		for(double elem : array){
			if(first){
				sb.append(elem);
				first = false;
			}
			else{
				sb.append(", " + elem);
			}
		}
		sb.append("]");
		return sb.toString();
	}
	
	
	public static double[] deleteEntryAt(double[] x, int index){
		
		double[] output = new double[x.length-1];
		for(int i = 0, k = 0; i<x.length; i++){
			if(i==index){
				continue;
			}
			output[k++] = x[i];
		}
		
		return output;
	}
	
	public static String[] deleteEntryAt(String[] x, int index){
		
		String[] output = new String[x.length-1];
		for(int i = 0, k = 0; i<x.length; i++){
			if(i==index){
				continue;
			}
			output[k++] = x[i];
		}
		
		return output;
	}
	
	
	/**
	 * calculates the sigmoid probability,	betas have to be in order with intercept at position 0
	 * sigmoid function :=   1 / (1 + e^-z)		z := b0 + b1*x1 + b2*x2 + ... + bn*xn
	 * @param x
	 * @param betas
	 * @return
	 */
	public static double sigmoid(double[] x, double[] betas){
		
		testForLength(x, betas);
		
		double z = betas[0];
		
		for(int i = 1 ; i<betas.length; i++){
			z += x[i-1] * betas[i];
		}
		
		return 1.0 / (1.0 + Math.exp(-z));
	}
	
	
	/**
	 * 	calculates the log likelihood for a sample (instance)
	 * likelihood = y * log(P(y|x[], betas)) + (1-y) * log(1 - P(y|x[], betas))
	 * @param x values for x�s of a sample
	 * @param y class value of the sample
	 * @param betas	(coefficients)
	 * @return
	 */
	public static double log_likelihood(double[] x, double y, double[] betas){
		testForLength(x, betas);
		double sigmoidProb = sigmoid(x, betas);
		
		double log_likelihood = y * Math.log(sigmoidProb) + (1-y) * Math.log(1-sigmoidProb);
		
		return log_likelihood;
	}
	

	
	
	/**
	 * calculates the log likelihood of the coefficients given the samples of the clients
	 * @return
	 */
	public static double log_likelihood(ArrayList<Double> clientsLL){
		
		double ll_total = 0;
		
		for(Double ll : clientsLL){		// the total log likelihood is the ll of every client added up
			ll_total += ll;
		}
		
		return ll_total;
	}
	
	
	
	
	/**
	 * calculates and returns the gradient of a sample
	 * the gradient is a vector containing the partial derivatives of the log likelihood function
	 * so every row is partially derived after one of the betas and the value of the this derivative is calculated using the data of the sample
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix gradient(double[] x, double y, double[] betas){
		
		DenseMatrix gradient = new DenseMatrix(betas.length, 1);
		
		double sigmoidProb = sigmoid(x,betas);
		
		for(int i = 0; i<betas.length - 1; i++){
			gradient.set(i, 0, (y - sigmoidProb) * x[i]);
		}
		
		gradient.set(betas.length - 1, 0, (y-sigmoidProb));	// the intercept has no x, so * x[i] becomes * 1
		
		return gradient;
	}
	
	

	
	/**
	 * Calculates and returns the joint gradient of the clients
	 * The gradient is a vector containing the partial derivatives of the log likelihood function
	 * so every row is the partial derivative of one of the betas 
	 * and the value of the this derivative is calculated using the data of the clients
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix gradient(ArrayList<DenseMatrix> clientsGradients){
	
		DenseMatrix totalGradient = new DenseMatrix(clientsGradients.get(0).numRows(), 1);
		
	
		for(DenseMatrix gradient : clientsGradients){		// the gradient of each client gets added up to obtain the total Gradient
			totalGradient = (DenseMatrix) totalGradient.add(gradient);
		}

		return totalGradient;
	}
	
	
	
	/**
	 * returns the Hessian matrix of a sample, which is a square matrix of second order partial derivatives
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix hessian(double[] x, int y, double[] betas){
		DenseMatrix hessian = new DenseMatrix(betas.length, betas.length);
		double sigmoidProb = sigmoid(x,betas);
		for(int i = 0; i<betas.length; i++){
			for(int j = 0; j<betas.length; j++){
				double xi;
				double xj;
				if(i == betas.length-1){
					xi = 1;
				}
				else{
					xi = x[i];
				}
				if(j == betas.length-1){
					xj = 1;
				}
				else{
					xj = x[j];
				}
				
				hessian.set(i, j, sigmoidProb * (1 - sigmoidProb) * xi * xj);
			}
		}

		return hessian;
	}

	
	
	/**
	 * returns the joint Hessian matrix of the clients, which is a square matrix of second order partial derivatives
	 * hessian is betas.length x betas.length
	 * @param x
	 * @param y
	 * @param betas
	 * @return
	 */
	public static DenseMatrix hessian(ArrayList<DenseMatrix> clientsHessian){
		
		DenseMatrix totalHessian = new DenseMatrix(clientsHessian.get(0).numRows(), clientsHessian.get(0).numColumns());
		
		for(DenseMatrix hessian : clientsHessian){
			
			totalHessian = (DenseMatrix) totalHessian.add(hessian);
		}
			
		return totalHessian;
	}
	
	
	/**
	 * tests if the number of betas equals the number of x�s + 1
	 * @param x
	 * @param betas
	 */
	public static void testForLength(double[] x, double[]betas){
		if(x.length + 1 != betas.length){
			System.out.println("x length: " + x.length + "    betas length: " + betas.length);
			throw new RuntimeException("Arrays have the wrong length");
		}
	}
	
	
	public static String doubleArrayToString(double[][] array){
		StringBuilder sb = new StringBuilder();
		
		boolean first = true;
		
		for(int i = 0; i<array.length; i++){
			first = true;
			for(int j = 0; j<array[0].length; j++){
				if(first){
					sb.append(array[i][j]);
					first = false;
				}
				else{
					sb.append(", " + array[i][j]);
				}
			}
			
			sb.append("\n");
		}
		
		return sb.toString();
	}
	
	
	public static String matrixToString(Matrix x){
		
		double[][] d = matrixToDoubleArray(x);
	
		return doubleArrayToString(d);
	}
	
	
	public static double[][] matrixToDoubleArray(Matrix x){
		
		double[][] d = new double[x.numRows()][x.numColumns()];
			
		for(int i = 0; i<x.numRows(); i++){
			for(int j = 0; j<x.numColumns(); j++){
				d[i][j] = x.get(i, j);
			}
		}
		
		return d;
	}
	
	
	/**
	 * return the inverse of the input matrix
	 * @param A
	 * @return
	 */
	public static DenseMatrix inverseMatrix(DenseMatrix A){
		
		DenseMatrix I = Matrices.identity(A.numRows()); // Identitiy Matrix
		DenseMatrix inverse = new DenseMatrix(A.numRows(), A.numColumns());
		
		A.solve(I, inverse);	// not sure why it is not I.solve(A, inverse)
		
		return inverse;
	}
	
	public static UpperSymmDenseMatrix getCovarianceMatrix(ArrayList<UpperSymmDenseMatrix> clients_covMats){
		
		UpperSymmDenseMatrix total_covMat = new UpperSymmDenseMatrix(clients_covMats.get(0).numColumns());	// total covarianc matrix
		
		for(UpperSymmDenseMatrix covMat : clients_covMats){
			total_covMat.add(covMat);
		}
		
		return total_covMat;
	}
	

	
	/**
	 * reads in a file and when there is a negative number in that column it writes false in its place, when its positive it writes true
	 * @param colNr (colNr starts at 1)
	 */
	public static void transformColumnToBoolean(int colNr, String path, String newFilePath){
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(path));
			BufferedWriter bw = new BufferedWriter(new FileWriter(newFilePath));
			String line;
			
			while((line = br.readLine()) != null){
				String[] fields = line.split("\t");
				
				if(fields[0].equals("sample")){
					continue;
				}
				
				for(int i = 0; i<fields.length; i++){
					if(i == colNr){
						if(Integer.parseInt(fields[i]) < 0){
							bw.write("lt_zero" + "\t");
						}
						else{
							bw.write("gt_zero" + "\t");
						}
					}
					else{
						bw.write(fields[i] + "\t");
					}
					
				}
				
				bw.write("\n");
			}
			
			br.close();
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static void geneExpressionIntoArff(String arffPath, String gePath, int colNr){
		
		try {
			BufferedReader br = new BufferedReader(new FileReader(gePath));
			BufferedWriter bw = new BufferedWriter(new FileWriter(arffPath));
			String line;
			HashSet<String> uniqueLines = new HashSet<>();
			ArrayList<String[]> lines = new ArrayList<>();
			
			bw.write("@relation geneExpression" + "\n");
			
			while((line = br.readLine()) != null){
				if(line.startsWith("sample")){
					continue;
				}
				String[] fields = line.split("\t");
				String[] numbers = deleteEntryAt(fields, 0);
				if(!hasDifferentValues(numbers)){	// if all values in the line are the same it is useless and causes problems
					continue;
				}
				
				Pattern pattern = Pattern.compile("ENSG\\S*\\.\\d*\t(.*)");
		        Matcher matcher = pattern.matcher(line);
		        matcher.find();

		        if(!uniqueLines.contains(matcher.group(1))){
					uniqueLines.add(matcher.group(1));
					lines.add(fields);
				}
				
			}
			
			// write attributes
			for(int i = 0; i<lines.size(); i++){
				bw.write("@attribute \"" + lines.get(i)[0] + "\" ");
				if(i == colNr){
					bw.write("{\"gt_zero\", \"lt_zero\"}\n");
				}
				else{
					bw.write("numeric\n");
				}
			}
			br.close();
			bw.write("@data\n");
			
			//write data
			for(int i = 1; i<lines.get(0).length; i++){	// first line is the gene name, so it starts at 1
				for(int j = 0; j<lines.size(); j++){
					if(j == colNr){
						if(Double.parseDouble(lines.get(j)[i]) < 0){
							bw.write("lt_zero\t");
						}
						else{
							bw.write("gt_zero\t");
						}
					}
					else{
						bw.write(lines.get(j)[i] + "\t");
					}
				}
				bw.write("\n");
			}
			
			bw.flush();
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * returns true if the numbers in the array have a different value and false if they all have the same
	 * @param numbers
	 * @return
	 */
	public static boolean hasDifferentValues(String[] numbers){
		
		if(numbers.length == 0){
			throw new RuntimeException("array is empty");
		}
		double firstValue = Double.parseDouble(numbers[0]);
		for(int i = 1; i<numbers.length; i++){
			if(firstValue != Double.parseDouble(numbers[i])){
				return true;
			}
		}
		
		
		return false;
	}
	
	
	
	public static double correlationCoefficient(double[] x, double[] y){
		double correlationCoefficient = 0;
		
		double E_x = 0;
		double E_y = 0;
		
		for(int i = 0; i<x.length; i++){
			E_x += x[i];
			E_y += y[i];
		}
		
		E_x /= x.length;
		E_y /= y.length;
		
		double counter = 0;	// Zaehler
		double denominator = 0; // Nenner
		
		double SSDx = 0;	// sum of squared deviations
		double SSDy = 0;	
		
		for(int i = 0; i<x.length; i++){
			counter += (x[i] - E_x) * (y[i] - E_y);
			SSDx += Math.pow(x[i] - E_x, 2);
			SSDy += Math.pow(y[i] - E_y, 2);
		}

		denominator = Math.sqrt(SSDx * SSDy);
		
		correlationCoefficient = counter / denominator;
		
		return correlationCoefficient;
	}
	
	
	
	public static boolean matricesHaveSameDimensions(ArrayList<DenseMatrix> matrices){
		if(matrices.size() == 0){
			return true;
		}
		
		int numCols = matrices.get(0).numColumns();
		int numRows = matrices.get(0).numRows();
		
		for(int i = 1; i<matrices.size(); i++){
			if(matrices.get(i).numColumns() != numCols || matrices.get(i).numRows() != numRows){
				return false;
			}
		}
		
		return true;
	}



	
	
	public static double likelihoodDifference(double old_ll, double new_ll){
		
		if(old_ll == Double.MAX_VALUE || new_ll == Double.MAX_VALUE){
			return Double.MAX_VALUE;
		}
		
		return Math.abs(new_ll - old_ll); 
	}
	
}