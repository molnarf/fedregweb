import java.util.ArrayList;

import no.uib.cipr.matrix.DenseMatrix;
import no.uib.cipr.matrix.UpperSymmDenseMatrix;

public class Main {
	
	private static final String username = "superuser";
	private static final String password = "test4321";  
	
	
	/**
	 * performs one iteration step to calculate the betas and updates the model of the server
	 * @param args
	 * @param new_ll, this new_ll becomes the old_ll in the next iteration
	 */
	public static void performIteration(String[] args, double new_ll){
		String project_id = args[0];
		
		String data_url = "http://127.0.0.1:8000/logistic_data/" + project_id  + "/";
		String model_url = "http://127.0.0.1:8000/logistic_model/" + project_id  + "/";
		String ll_url = "http://127.0.0.1:8000/logistic_ll/" + project_id  + "/";
		
		
		ArrayList<UserData> users = ApiUtils.getUserDataFromServer(username, password, data_url);
		
		ArrayList<DenseMatrix> hessians = new ArrayList<>();
		ArrayList<DenseMatrix> gradients = new ArrayList<>();
		ArrayList<Double> log_likelihoods = new ArrayList<>();
		
				
		for(UserData data : users){
			hessians.add(data.getHessian());
			gradients.add(data.getGradient());
			log_likelihoods.add(data.getLog_likelihood());
		}

		// update the old and new likelihoods
		double old_ll = new_ll;
		new_ll = MyUtils.log_likelihood(log_likelihoods);
		ApiUtils.postLikelihoodsToServer(username, password, ll_url, old_ll, new_ll);
		
		
		double[] currentModel = ApiUtils.getModelFromServer(username, password, model_url);
		
		
		// class index could be received from attribute name later on 
		LRmodel LRM = new LRmodel(gradients, hessians, hessians.get(0).numColumns()-1, currentModel);
		LRM.federatedCoefficientCalculation();

		ApiUtils.postModelToServer(username, password, model_url, LRM.betas);
		
	}
	
	
	public static void calculatePvalues(String[] args){
		String project_id = args[0];
		
		String model_url = "http://127.0.0.1:8000/logistic_model/" + project_id  + "/";
		String covMat_url = "http://127.0.0.1:8000/logistic_covMatrix/" + project_id  + "/";
		String p_value_url = "http://127.0.0.1:8000/logistic_p_value/" + project_id  + "/";
		
		ArrayList<UpperSymmDenseMatrix> covMats = ApiUtils.getCovMatFromServer(username, password, covMat_url);
		double[] currentModel = ApiUtils.getModelFromServer(username, password, model_url);
		
		LRmodel LRM = new LRmodel(covMats, currentModel,covMats.get(0).numColumns()-1);
		LRM.calculateFederatedPvalues();
		
		// also posts the z-values and the standard errors
		ApiUtils.postPvaluesToServer(username, password, p_value_url, LRM.p_values, LRM.z_values, LRM.std_errs);
	}
	

	public static void main(String[] args){

		String project_id = args[0];
		
		String project_ll_url = "http://127.0.0.1:8000/logistic_ll/" + project_id  + "/";
		
		
		double[] lls_old_new = ApiUtils.getLikelihoodsFromServer(username, password, project_ll_url);
		
		if(MyUtils.likelihoodDifference(lls_old_new[0], lls_old_new[1]) > 0.00000000001){	
			// if the diff is larger than the converge diff
			// this means, that the model did not converge yet
			
			performIteration(args, lls_old_new[1]);
		}
		else{	// when the model has converged and the p-values need to be calculated
			calculatePvalues(args);
		}
		
		
		
	}
	
}
