from rest_framework import serializers
from .models import LassoInit, Lasso, Project, LambdaInit, LassoUpdate
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User


class LassoInitSerializer(serializers.Serializer):
    z_vector = serializers.CharField()
    class_sum = serializers.FloatField()
    num_instances = serializers.IntegerField()
    attributes = serializers.CharField()

    def create(self, validated_data):
        return LassoInit.objects.create(**validated_data)


class LambdaInitSerializer(serializers.Serializer):
    max_lambda_vector = serializers.CharField()

    def create(self, validated_data):
        return LambdaInit.objects.create(**validated_data)


class LassoUpdateSerializer(serializers.Serializer):
    index = serializers.IntegerField()
    r_sum = serializers.FloatField()
    iteration = serializers.IntegerField()

    def create(self, validated_data):
        return LassoUpdate.objects.create(**validated_data)


class LassoAuthenticationSerializer(serializers.Serializer):
    username = serializers.CharField();
    pw = serializers.CharField();
    slug = serializers.CharField();

    def validate(self, data):
        project = get_object_or_404(Project, slug=data["slug"])

        # checks if the project has the rigth regression type
        if (project.regression_type != "LA"):
            raise serializers.ValidationError("no lasso")

        user = get_object_or_404(User, username=data["username"])

        # checks if user and pw fit together
        if (not user.check_password(data["pw"])):
            raise serializers.ValidationError("wrong pw")

        # checks if user is part of the project
        if (user not in project.participants.all()):
            raise serializers.ValidationError("not in project")

        return data
