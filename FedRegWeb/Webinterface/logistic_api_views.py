from django.shortcuts import render, redirect
from django.shortcuts import get_object_or_404
from .models import Project, Logistic, Logistic_data, CovarianceMatrix, Evaluate
from django.http import JsonResponse
from rest_framework.decorators import api_view, authentication_classes
from .logistic_serializers import LogisticDataSerializer, CovarianceMatrixSerializer
from .logisticAuthenticationSerializer import LogisticAuthenticationSerializer
from rest_framework import viewsets
import json
import subprocess
import time
from rest_framework_simplejwt.authentication import JWTAuthentication
from django.contrib.auth.models import User
import math
import os
import mimetypes
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.core.files.storage import FileSystemStorage
from .logistic_functions import evaluate_dataset



#used for the local clients to submit their data and get the regression model back
@api_view(["POST"])
def logisticView(request, slug):
    request.data["slug"] = slug
    authentication_serializer = LogisticAuthenticationSerializer(data = request.data)
    if(authentication_serializer.is_valid()):

        if request.method == "POST":
            project = get_object_or_404(Project, slug=slug)
            logistic_model = project.logistic
            current_iteration = logistic_model.iteration
            current_time = time.time()

            logistic_data = Logistic_data.objects.create(regression = logistic_model, hessian = request.data["hessian"],
                                                         gradient = request.data["gradient"],
                                                         log_likelihood = request.data["log_likelihood"],
                                                         timestamp = current_time,
                                                         attribute_names = request.data["attribute_names"])
            logistic_data.save()

            user = get_object_or_404(User, username=request.data["username"])
            if user not in project.participants_ready.all():
                project.participants_ready.add(user)

            if len(logistic_model.data.all()) == len(project.participants.all()) and project.is_running:
                #the function returns the new model as JsonResponse
                return calculateNewModel(slug, logistic_model, current_time)


            while current_iteration != logistic_model.iteration - 1:
                time.sleep(1)
                project = get_object_or_404(Project, slug=slug)  # get the updated model
                logistic_model = project.logistic

            return JsonResponse({'regression_model': logistic_model.regression_model, })




def calculateNewModel(slug, logistic_model, current_time):
    # in case 2 clients save their data on the same time
    # and both would get in this if statement
    latest_data = Logistic_data.objects.filter(regression=logistic_model).latest("timestamp")
    if latest_data.timestamp == current_time:
        if logistic_model.attribute_names in [None, '']:  # i put this here to avoid lost updates
            logistic_model.attribute_names = "Intercept, " + latest_data.attribute_names
            logistic_model.save()


        jar_path = 	os.path.dirname(os.path.realpath(__file__)) + "/logistic_jar/Logistic_Server.jar"
        subprocess.call(["java", "-jar", jar_path, slug])

        project = get_object_or_404(Project, slug=slug)  # get the updated model
        logistic_model = project.logistic

        logistic_model.data.all().delete()
        logistic_model.covMat.all().delete()
        logistic_model.iteration = logistic_model.iteration + 1

        logistic_model.save()

        return JsonResponse({'regression_model': logistic_model.regression_model, })




# used to get and post the model (betas) in the server jar
@api_view(["GET", "POST"])
@authentication_classes([JWTAuthentication])
def logisticModelView(request, slug):
    if request.user.username != "superuser":    #this user is used in the server jar
        return None

    project = get_object_or_404(Project, slug=slug)
    logistic_model = project.logistic


    if request.method == "POST":
        form_data = json.loads(request.body.decode())  # when the post data is in the body (from java)
        logistic_model.regression_model = form_data["regression_model"]
        logistic_model.save()

    return JsonResponse({"regression_model": logistic_model.regression_model})


# used to post the p-values (from the server jar)
@api_view(["GET", "POST"])
@authentication_classes([JWTAuthentication])
def pValueView(request, slug):
    if request.user.username != "superuser":
        return None

    project = get_object_or_404(Project, slug=slug)
    logistic_model = project.logistic

    if request.method == "POST":
        form_data = json.loads(request.body.decode())  # when the post data is in the body (from java)
        logistic_model.p_values = form_data["p_values"]
        logistic_model.z_values = form_data["z_values"]
        logistic_model.std_errs = form_data["std_errs"]
        logistic_model.finished = True
        logistic_model.save()
        # if this view gets called by the server jar, the model already converged and the results file can be written
        writeResultsFile(project)

    return JsonResponse({"p_values": logistic_model.p_values})


# write the results file for download, after the model has converged
def writeResultsFile(project):
    filename = "Webinterface/logistic_results/" + project.slug + "_results.tsv"
    logistic_model = project.logistic
    result_file = open(filename, 'w+')
    result_file.write("Variable\tCoefficient\tOdds_Ratio\tStd_Err\tz-value\tp-value\n")

    table = []

    names = logistic_model.attribute_names.split(", ")
    coefficients = logistic_model.regression_model.split(", ")
    std_errs = logistic_model.std_errs.split(", ")
    z_values = logistic_model.z_values.split(", ")
    p_values = logistic_model.p_values.split(", ")
    OR = []
    for beta in coefficients:
        OR.append(math.exp(float(beta)))

    table.append(names)
    table.append(coefficients)
    table.append(OR)
    table.append(std_errs)
    table.append(z_values)
    table.append(p_values)

    for j in range(len(table[0])-1):
        for i in range(len(table)):
            result_file.write(str(table[i][j]) + "\t")
        result_file.write("\n")

    result_file.close()




# used in the server jar to get the data of the clients
class LogisticDataView(viewsets.mixins.ListModelMixin, viewsets.GenericViewSet):
    serializer_class = LogisticDataSerializer

    authentication_classes = [JWTAuthentication]


    def get_queryset(self):
        if self.request.user.username != "superuser":
            return None
        project = get_object_or_404(Project, slug=self.kwargs['slug'])
        logistic_model = project.logistic
        return logistic_model.data


# used to post the covariance matrix from the user and to list all matrices for the server jar
class CovarianceMatrixView(viewsets.mixins.ListModelMixin, viewsets.mixins.CreateModelMixin, viewsets.GenericViewSet):

    serializer_class = CovarianceMatrixSerializer

    def get_queryset(self):
        project = get_object_or_404(Project, slug=self.kwargs['slug'])
        logistic_model = project.logistic
        return logistic_model.covMat

    def post(self, request, *args, **kwargs):
        covMatSerializer = CovarianceMatrixSerializer(data = request.data)
        project = get_object_or_404(Project, slug=self.kwargs['slug'])
        logistic_model = project.logistic
        if(covMatSerializer.is_valid()):
            covMatSerializer.save(regression = logistic_model)
            return JsonResponse(covMatSerializer.data)



# used to post the updated likelihoods (called from the user(get) and server(post))
@api_view(["GET", "POST"])
def logistic_llView(request, slug):
    project = get_object_or_404(Project, slug=slug)
    logistic_model = project.logistic

    if request.method == "POST":
        request.data["slug"] = slug
        authentication_serialier = LogisticAuthenticationSerializer(data=request.data)
        if (authentication_serialier.is_valid()):
            logistic_model.log_likelihood_old = request.data["log_likelihood_old"]
            logistic_model.log_likelihood_new = request.data["log_likelihood_new"]
            logistic_model.save()

    return JsonResponse({"log_likelihood_old": logistic_model.log_likelihood_old, "log_likelihood_new": logistic_model.log_likelihood_new})


#download the trained model file
def download_file(request, slug):
    # fill these variables with real values

	fl_path = "logistic_results/" + slug + "_results.tsv"
	dir_path = os.path.dirname(os.path.realpath(__file__)) + "/logistic_results/"
	filename = slug + "_results.tsv"
	file_path = os.path.join(dir_path, filename)

	fl = open(file_path, "r")
	mime_type, _ = mimetypes.guess_type(fl_path)
	response = HttpResponse(fl, content_type=mime_type)
	response['Content-Disposition'] = "attachment; filename=%s" % filename
	return response


#gets called when the upload button is pressed and starts evaluating the uploaded dataset
def upload(request, slug):
    project = get_object_or_404(Project, slug=slug)
    logistic_model = project.logistic
    pk = project.id
    if request.method == 'POST':
        uploaded_file = request.FILES['document']
        fs = FileSystemStorage()
        name = fs.save(uploaded_file.name, uploaded_file)
        result = evaluate_dataset(logistic_model, fs.location, name)
        eval = Evaluate.objects.create(regression=logistic_model, result=result,
                                       data_owner=request.user, filename=name.split(".")[0])
        eval.save()
        logistic_model.evaluate.add(eval)

    return redirect("project-detail", pk)


# used to link the result of the evaluated dataset to the correct url
def evaluated_dataset(request, slug):
    result = get_object_or_404(Evaluate, filename=slug)
    return render(request,"Webinterface/evaluated_LO.html",context={"result": result})