from django.shortcuts import get_object_or_404
from .models import Project, Lasso, LassoInit
import json
import time


def lasso_init(slug):
    # get objects
    lasso = Project.objects.get(slug=slug).lasso
    lasso_inits = lasso.lassoinit_set.all()

    model_z_vector = []
    class_sum = 0
    num_instances = 0
    model_attributes = []

    # iterate over lasso_inits
    for lasso_init in lasso_inits:

        class_sum += lasso_init.class_sum
        num_instances += lasso_init.num_instances

        if (len(model_attributes) == 0):
            model_attributes = lasso_init.attributes.split(";")

        # convert Strings to float lists
        z_vector_list = lasso_init.z_vector.split(";")
        z_vector_list = list(map(float, z_vector_list))

        if (len(model_z_vector) == 0):
            model_z_vector = [0] * len(z_vector_list)

        for i in range(len(z_vector_list)):
            model_z_vector[i] += z_vector_list[i]

    # delete the lasso inits
    lasso_inits.delete()

    mean = class_sum / num_instances

    coefficients = [0] * len(model_z_vector)
    coefficients.append(mean)

    # save changes
    lasso.attributes = json.dumps(model_attributes)
    lasso.mean = mean
    lasso.num_instances = num_instances
    lasso.coefficients = coefficients
    lasso.z_vector = json.dumps(model_z_vector)
    lasso.is_initialized = True
    lasso.save(update_fields=["z_vector", "is_initialized", "mean", "num_instances", "coefficients", "attributes"])


def lambda_init(slug):
    # get the objects
    lasso = Project.objects.get(slug=slug).lasso
    lambda_inits = lasso.lambdainit_set.all()

    model_max_lambda_vector = []

    # iterate over lambda_inits
    for lambda_init in lambda_inits:

        # convert Strings to float lists
        max_lambda_vector = lambda_init.max_lambda_vector.split(";")
        max_lambda_vector = list(map(float, max_lambda_vector))

        if (len(model_max_lambda_vector) == 0):
            model_max_lambda_vector = [0] * len(max_lambda_vector)

        for i in range(len(max_lambda_vector)):
            model_max_lambda_vector[i] += max_lambda_vector[i]

    lambda_inits.delete()

    model_max_lambda = 0;
    for max_lambda in model_max_lambda_vector:
        if abs(max_lambda) > model_max_lambda:
            model_max_lambda = abs(max_lambda)

    print("max labda: " + str(model_max_lambda))

    lasso.l = model_max_lambda * lasso.normalized_l
    lasso.runtime = time.time()
    lasso.save(update_fields=["l","runtime"])


def lasso_update(slug, iteration):
    # get the objects
    lasso = Project.objects.get(slug=slug).lasso
    lasso_updates = lasso.lassoupdate_set.all().filter(iteration=iteration)

    index = lasso_updates[0].index

    r_sum = 0

    # add up the r_sums
    for lasso_update in lasso_updates:
        if (lasso_update.index != index):
            print("inconsisten indices!")
        r_sum += lasso_update.r_sum

    # delete the update object
    lasso_updates.delete()

    # get coefficients
    jsonDec = json.decoder.JSONDecoder()
    coefficients = jsonDec.decode(lasso.coefficients)
    old_coefficient = coefficients[index]

    # calc new coefficient
    new_coefficient = 0

    if (index == len(coefficients) - 1):
        # make intersect update
        print("intersect update")
        new_coefficient = intersect_update(lasso.num_instances, r_sum, old_coefficient)
    else:
        # make beta update
        z_vector = jsonDec.decode(lasso.z_vector)
        z = z_vector[index]
        new_coefficient = beta_update(old_coefficient, r_sum, lasso.l, z)
    # print(new_coefficient)

    # calc the change
    change = old_coefficient - new_coefficient

    # test if continue updating coefficient
    max_percent_change = lasso.max_percent_change
    keep_updating = True
    finished = False

    if (index == len(coefficients) - 1):
        if (old_coefficient != 0):
            change_percent = abs(change) / abs(old_coefficient)
            if change_percent > max_percent_change:
                max_percent_change = change_percent
        else:
            if abs(change) > max_percent_change:
                max_percent_change = abs(change)
        if(max_percent_change < 0.1):
            finished = True
            lasso.runtime=time.time()-lasso.runtime
			
        max_percent_change = 0
    else:
        if (new_coefficient == 0 and old_coefficient == 0):
            keep_updating = False
        if (old_coefficient != 0):
            change_percent = abs(change) / abs(old_coefficient)
            if change_percent > max_percent_change:
                max_percent_change = change_percent
        else:
            if abs(change) > max_percent_change:
                max_percent_change = abs(change)

    print("Index: " + str(index))
    print("Old Coefficient: " + str(old_coefficient))
    print("New Coefficient: " + str(new_coefficient))
    print("Change: " + str(change))
    print("Keep updating: " + str(keep_updating))

    # save changes
    coefficients[index] = new_coefficient

    iteration = lasso.iteration
    iteration += 1

    lasso.max_percent_change = max_percent_change
    lasso.finished = finished
    lasso.keep_updating = keep_updating
    lasso.iteration = iteration
    lasso.coefficients = coefficients
    lasso.change = change

    lasso.save(update_fields=["coefficients", "change", "iteration", "finished", "keep_updating", "max_percent_change","runtime"])


def intersect_update(num_instances, r_sum, old_intersect):
    var = r_sum / num_instances;
    new_intersect = old_intersect + var;
    return new_intersect


def beta_update(old_coefficient, r_sum, l, z):
    new_beta_p = 0
    new_beta_n = 0

    r_sum = r_sum * -1

    new_beta_n = old_coefficient - (r_sum - l) / z;
    new_beta_p = old_coefficient - (r_sum + l) / z;

    if (new_beta_n > 0):
        new_beta_n = 0;

    if (new_beta_p < 0):
        new_beta_p = 0;

    if (new_beta_n != 0):
        return new_beta_n
    elif (new_beta_p != 0):
        return new_beta_p
    return 0






