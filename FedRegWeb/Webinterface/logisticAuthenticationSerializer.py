from rest_framework import serializers
from .models import Project
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User




class LogisticAuthenticationSerializer(serializers.Serializer):
    username = serializers.CharField();
    password = serializers.CharField();
    slug = serializers.CharField();

    def validate(self, data):
        project = get_object_or_404(Project, slug=data["slug"])

        # checks if the project has the rigth regression type
        if (project.regression_type != "LO"):
            raise serializers.ValidationError("no logistic project")

        user = get_object_or_404(User, username=data["username"])

        # this means the request comes from the server and it is allowed
        if data["username"] == "superuser" and user.check_password(data["password"]):
            return data

        # checks if user and pw fit together
        if (not user.check_password(data["password"])):
            raise serializers.ValidationError("wrong password")

        # checks if user is part of the project
        if (user not in project.participants.all()):
            raise serializers.ValidationError("not in project")

        return data