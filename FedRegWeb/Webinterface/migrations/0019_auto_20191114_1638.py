# Generated by Django 2.2.5 on 2019-11-14 15:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Webinterface', '0018_logistic_data_attribute_names'),
    ]

    operations = [
        migrations.CreateModel(
            name='CovarianceMatrix',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('covMatrix', models.TextField()),
                ('regression', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='covMat', to='Webinterface.Logistic')),
            ],
        ),
        migrations.DeleteModel(
            name='InverseCovarianceMatrix',
        ),
    ]
