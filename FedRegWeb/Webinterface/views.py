from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from .forms import NewUserForm, LassoStartForm
#from rest_framework import viewsets
from django.contrib.auth.decorators import login_required
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import get_object_or_404
from .models import Project, Logistic_data
from . import logistic_api_views
import mimetypes
import os
from FedRegWeb import settings
from django.http import JsonResponse
from rest_framework.decorators import api_view




def homepage(request):
    return render(request=request,              # "variable=variable" is rendundant and could just be "variable" as seen below
                  template_name="Webinterface/homepage.html",
                  )

def downloads(request):
	return render(request, "Webinterface/downloads.html")


def download_local(request):
    fl_path = os.path.join(settings.BASE_DIR,"Webinterface/downloads/GUI.zip")

    filename = "GUI.zip"

    fl = open(fl_path, "rb")
    mime_type, _ = mimetypes.guess_type(fl_path)
    response = HttpResponse(fl, content_type=mime_type)
    response['Content-Disposition'] = "attachment; filename=%s" % filename
    return response




def register(request):

    if request.method == "POST":        # handle user input
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            username = form.cleaned_data.get("username")
            messages.success(request, f"New Account Created: {username}")
            login(request, user)
            messages.info(request, f"You are now logged in as {username}")
            return redirect("homepage")
        else:
            for msg in form.error_messages:
                messages.error(request, f"{msg}: form.error_messages[msg]")


    form = NewUserForm
    return render(request,
                  "Webinterface/register.html",
                  context={"form":form})


@login_required
def projects(request):
	current_user = request.user
	own_projects = current_user.own_projects.all()
	participant_projects = current_user.participant_projects.all()
	context = {"own_projects":own_projects,"participant_projects":participant_projects}
	return render(request, "Webinterface/projects.html",context)
	
@login_required
def join(request,slug):
	project = get_object_or_404(Project, slug=slug)
	if request.method == "POST":
		if request.user not in project.participants.all():
			project.participants.add(request.user)
			messages.info(request, f"You joined the project {project.name}")
		else:
			messages.info(request, f"You are already participating in this project")
		return HttpResponseRedirect("/project/{id}/".format(id= project.id))
	else:
		context = {"project":project}
		return render(request, "Webinterface/project_join.html",context)


@login_required
def start(request, pk):
	project = get_object_or_404(Project, pk=pk)
	if (request.user != project.supervisor):
		raise Http404("Page not found")

	project.participants.set(project.participants_ready.all())
	project.save()

	if (project.regression_type == "LA"):
		if (request.method == "POST"):
			form = LassoStartForm(request.POST)
			if form.is_valid():
				lasso = project.lasso
				lasso.normalized_l = form.cleaned_data.get("lambda_value")
				lasso.save(update_fields=["normalized_l"])
				project.is_running = True
				project.save(update_fields=["is_running"])
				return redirect("project-detail", pk)

		form = LassoStartForm(initial={'lambda_value': 0})
		return render(request, "Webinterface/lasso_start.html", context={"form": form})


	project.is_running = True
	project.save(update_fields=["is_running"])

	if project.regression_type == "LO":
		latest_data = Logistic_data.objects.filter(regression=project.logistic).latest("timestamp")
		logistic_api_views.calculateNewModel(project.slug, project.logistic, latest_data.timestamp)

	return redirect("project-detail",pk)



class ProjectCreateView(LoginRequiredMixin,  CreateView):
	model = Project
	fields = ["name","regression_type"]
	template_name = "Webinterface/project_form.html"
	
	def form_valid(self,form):
		form.instance.supervisor = self.request.user
		return super().form_valid(form)
		
	
		
class ProjectDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
	model = Project
	template_name = "Webinterface/project_detail.html"
	
	def test_func(self):
		project = self.get_object()
		if self.request.user == project.supervisor:
			return True
		if self.request.user in project.participants.all():
			return True
		return False
		
class ProjectDeleteView(LoginRequiredMixin, UserPassesTestMixin, DeleteView):
	model = Project
	success_url = "/projects/"

	def test_func(self):
		project = self.get_object()
		if self.request.user == project.supervisor:
			return True
		return False




def accountView(request):
    if request.method == "GET":
        user = request.user
        return render(request,
                      "Webinterface/account.html",
                      context={"object": user})


@api_view(["GET"])
def getType(request, slug):
	project = get_object_or_404(Project, slug=slug)
	type = {"type": project.regression_type}

	return JsonResponse(type)