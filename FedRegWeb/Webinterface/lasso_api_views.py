from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .lasso_serializers import LassoInitSerializer, LassoAuthenticationSerializer, LambdaInitSerializer, \
    LassoUpdateSerializer
from rest_framework import status
import json
from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Project, Lasso
from . import lasso_functions
import time
from django.contrib.auth.models import User


@api_view(["POST"])
def lasso_init(request, slug):
    auth = LassoAuthenticationSerializer(data=request.data)

    # checks if the user is authenticated
    if (auth.is_valid()):

        # print(auth.validated_data)
        project = get_object_or_404(Project, slug=slug)
        init_serializer = LassoInitSerializer(data=request.data)

        # check if data is valid
        if (init_serializer.is_valid()):

            user = get_object_or_404(User, username=request.data["username"])
            if user not in project.participants_ready.all():
                project.participants_ready.add(user)

            print(auth.data["username"] + " started lasso init")

            # only for development:
            project.is_running = False
            project.save()
            lasso = project.lasso
            lasso.is_initialized = False
            lasso.iteration = -1
            lasso.finished = False
            lasso.save(update_fields=["is_initialized", "iteration", "finished"])


            # print(init_serializer.validated_data)
            # saves the object
            init_serializer.save(lasso_project=project.lasso)

            # wait until the project is initialized
            while (not Project.objects.get(slug=slug).lasso.is_initialized):
                time.sleep(10)

            data = {"mean": Project.objects.get(slug=slug).lasso.mean}
            return JsonResponse(data)
        # return JsonResponse(init_serializer.data)

        else:
            return JsonResponse(init_serializer.errors)

    else:
        return (JsonResponse(auth.errors))


@api_view(["POST"])
def lambda_init(request, slug):
    auth = LassoAuthenticationSerializer(data=request.data)

    # checks if the user is authenticated
    if (auth.is_valid()):

        project = get_object_or_404(Project, slug=slug)
        lambda_serializer = LambdaInitSerializer(data=request.data)

        # check if data is valid
        if (lambda_serializer.is_valid()):

            lambda_serializer.save(lasso_project=project.lasso)
            return JsonResponse(lambda_serializer.data)

        else:
            return JsonResponse(lambda_serializer.errors)


    else:
        return (JsonResponse(auth.errors))


@api_view(["POST"])
def lasso_update(request, slug):
    auth = LassoAuthenticationSerializer(data=request.data)

    # checks if the user is authenticated
    if (auth.is_valid()):

        project = get_object_or_404(Project, slug=slug)
        update_serializer = LassoUpdateSerializer(data=request.data)
        lasso_pk = project.lasso.pk

        # check if data is valid
        if (update_serializer.is_valid()):
            update_serializer.save(lasso_project=project.lasso)
            iteration = update_serializer.data["iteration"]

            while (not Lasso.objects.get(pk=lasso_pk).iteration == iteration):
                time.sleep(1)
            lasso = Lasso.objects.get(pk=lasso_pk)
            data = {"change": lasso.change, "keep_updating": lasso.keep_updating, "finished": lasso.finished}
            return JsonResponse(data)

        else:
            return JsonResponse(update_serializer.errors)


    else:
        return (JsonResponse(auth.errors))
