import math


def evaluate_dataset(logistic_model, file_path, filename):
    file = open(file_path + "/" + filename, "r")

    headertext = ""
    text = ""

    header = logistic_model.attribute_names
    header = header.split(", ")
    header = header[1:len(header)-1]


    text += "\n\n"
    line = file.readline()

    betas = logistic_model.regression_model
    betas = betas.split(", ")

    while line:
        if not (line.startswith("%") or line.startswith("@")):
            line = line.strip("\n")
            variables = line.split(",")
            print(variables)
            p = sigmoid(variables, betas)
            line = line.replace(",", "\t")
            text += line + "\t" + str(p) + "\n"
            if len(header) < len(variables):
                #append class name when present in the file
                header.append(logistic_model.attribute_names.split(", ")[len(logistic_model.attribute_names.split(", "))-1])

        line = file.readline()

    file.close()

    header.append("P(y=1)")
    for var in header:
        headertext += var + "\t"

    text = headertext + text

    return text

def sigmoid(variables, betas):
    sum = float(betas[0])
    for i in range(1, len(betas)):
        sum += float(betas[i])*float(variables[i-1])

    return round(1 / (1 + math.exp(-sum)), 4)