from rest_framework import serializers
from .models import Logistic_data, Logistic, CovarianceMatrix


# used in the server jar to get the data of the clients
class LogisticDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Logistic_data
        fields = ("hessian", "gradient", "log_likelihood")



# used for the calculation of the p-values
class CovarianceMatrixSerializer(serializers.ModelSerializer):
    class Meta:
        model = CovarianceMatrix
        fields = ["covMatrix"]
