"""FedRegWeb URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views
from .views import ProjectCreateView, ProjectDetailView, ProjectDeleteView
from . import lasso_api_views, logistic_api_views


urlpatterns = [
    path("", views.homepage, name= "homepage"),
    path("register/", views.register, name= "register"),
    path("login/", auth_views.LoginView.as_view(template_name="Webinterface/login.html"), name= "login"),
    path("logout/", auth_views.LogoutView.as_view(), name= "logout"),
    path("projects/", views.projects, name= "projects"),
	path("new/",ProjectCreateView.as_view(),name = "project-create"),
	path('project/<int:pk>/', ProjectDetailView.as_view(), name = "project-detail"),
	path("join/<slug:slug>/", views.join, name='project-join'),
	path("start/project/<int:pk>/", views.start, name='project-start'),
	path('project/<int:pk>/delete/', ProjectDeleteView.as_view(), name = "project-delete"),
	path("account/", views.accountView, name = "account"),
	path("type/<slug:slug>/", views.getType),

	path("logistic/<slug:slug>/", logistic_api_views.logisticView, name="logistic"),
	path("logistic_data/<slug:slug>/", logistic_api_views.LogisticDataView.as_view({'get': 'list'}), name="logistic_data"),
	path("logistic_model/<slug:slug>/", logistic_api_views.logisticModelView, name="logistic_model"),
	path("logistic_ll/<slug:slug>/", logistic_api_views.logistic_llView, name="logistic_ll"),
	path("logistic_covMatrix/<slug:slug>/", logistic_api_views.CovarianceMatrixView.as_view({"get": "list"}), name = "covMat"),
	path("logistic_p_value/<slug:slug>/", logistic_api_views.pValueView, name="p_value"),

	path("initLasso/<slug:slug>/",lasso_api_views.lasso_init, name ="lasso-init"),
	path("initLambda/<slug:slug>/",lasso_api_views.lambda_init, name ="lambda-init"),
	path("updateLasso/<slug:slug>/",lasso_api_views.lasso_update, name ="lasso-update"),

	path("downloads/", views.downloads, name = "downloads"),
	path("downloads/GUI.zip" , views.download_local),

	path("logistic_results/<slug:slug>/", logistic_api_views.download_file),
	path("project/<slug:slug>/upload/", logistic_api_views.upload, name='upload'),
	path("evaluated/<slug:slug>/", logistic_api_views.evaluated_dataset, name="evaluated"),

]

