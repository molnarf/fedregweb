from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.utils.crypto import get_random_string
from django.urls import reverse
import time;
import json;
# Create your models here.


class Linear(models.Model):
	placeholder = models.CharField(max_length=100, blank=True)


class Logistic(models.Model):
    regression_model = models.TextField()
    p_values = models.TextField()
    z_values = models.TextField(blank=True)
    std_errs = models.TextField(blank=True)
    attribute_names = models.TextField(blank=True)
    log_likelihood_old = models.CharField(max_length=1000, blank=True, default="")
    log_likelihood_new = models.CharField(max_length=1000, blank=True, default="")

    iteration = models.IntegerField(default=0)
    finished = models.BooleanField(default=0)


class Evaluate(models.Model):
	data_owner = models.ForeignKey(User, related_name = "owner", on_delete = models.CASCADE)
	regression = models.ForeignKey(Logistic, related_name= "evaluate", on_delete=models.CASCADE)
	result = models.TextField(blank=True)
	filename = models.CharField(max_length=100, blank=True)


class Logistic_data(models.Model):
    gradient = models.TextField()
    hessian = models.TextField()
    log_likelihood = models.CharField(max_length=1000, blank=True)
    timestamp = models.FloatField(default=0)
    attribute_names = models.TextField(blank=True)

    regression = models.ForeignKey(Logistic, related_name= "data", on_delete=models.CASCADE)


class CovarianceMatrix(models.Model):
	covMatrix = models.TextField()

	regression = models.ForeignKey(Logistic, related_name="covMat", on_delete=models.CASCADE)


class Lasso(models.Model):
	is_initialized = models.BooleanField(default=0)

	z_vector = models.TextField(blank=True)
	mean = models.FloatField(blank=True, null=True)
	num_instances = models.IntegerField(blank=True, null=True)

	# lambda
	normalized_l = models.FloatField(default=0)
	l = models.FloatField(default=0)

	attributes = models.TextField(blank=True)
	coefficients = models.TextField(blank=True)
	change = models.FloatField(default=0)
	iteration = models.IntegerField(default=-1)

	# convergence
	finished = models.BooleanField(default=0)
	keep_updating = models.BooleanField(default=1)
	max_percent_change = models.FloatField(default=0)

	runtime = models.FloatField(default=0)

	def html_out(self):
		jsonDec = json.decoder.JSONDecoder()
		coefficients = jsonDec.decode(self.coefficients)
		attributes = jsonDec.decode(self.attributes)

		result = "Used Lambda: " + str(self.normalized_l) + "</br>"
		result += attributes[len(attributes) - 1] + " = </br>"

		for i in range(0, len(coefficients) - 1):
			result += "{:12.4f}".format(round(coefficients[i], 4)) + " * " + attributes[i] + " +</br>"

		result += "{:12.4f}".format(round(coefficients[len(coefficients) - 1], 4)) + "</br>"

		return result


class LambdaInit(models.Model):
	lasso_project = models.ForeignKey(Lasso, on_delete=models.CASCADE)
	max_lambda_vector = models.TextField();
	timestamp = models.FloatField(blank=True, null=True)

	def save(self, *args, **kwargs):
		self.timestamp = time.time()
		super(LambdaInit, self).save(*args, **kwargs)


class LassoInit(models.Model):
	lasso_project = models.ForeignKey(Lasso, on_delete=models.CASCADE)
	z_vector = models.TextField()
	class_sum = models.FloatField()
	num_instances = models.IntegerField()
	timestamp = models.FloatField(blank=True, null=True)
	attributes = models.TextField()

	def save(self, *args, **kwargs):
		self.timestamp = time.time()
		super(LassoInit, self).save(*args, **kwargs)


class LassoUpdate(models.Model):
	lasso_project = models.ForeignKey(Lasso, on_delete=models.CASCADE)
	index = models.IntegerField()
	r_sum = models.FloatField()
	iteration = models.IntegerField(default=0)
	timestamp = models.FloatField(blank=True, null=True)

	def save(self, *args, **kwargs):
		self.timestamp = time.time()
		super(LassoUpdate, self).save(*args, **kwargs)


class Project(models.Model):

	Regression_Types = (
        ('LI', 'linear'),
        ('LO', 'logistic'),
		('LA', 'lasso'),
    )
	regression_type = models.CharField(max_length=2, choices=Regression_Types, default="LI")
	
	name = models.CharField(max_length=100)
	slug = models.SlugField(max_length=12,unique=True,blank = True)
	date_created = models.DateTimeField(default = timezone.now)
	
	supervisor = models.ForeignKey(User, related_name = "own_projects", on_delete = models.CASCADE)
	
	participants = models.ManyToManyField(User, related_name = "participant_projects", blank=True)
	participants_ready = models.ManyToManyField(User, related_name = "participant_projects_ready", blank=True)
	
	is_running = models.BooleanField(default=0)
	old_running = False

	
	linear = models.ForeignKey(
        Linear,
        on_delete=models.CASCADE,
		null = True,
		blank = True
    )
	
	logistic = models.ForeignKey(
        Logistic,
        on_delete=models.CASCADE,
		null = True,
		blank = True
    )
	
	lasso = models.ForeignKey(
        Lasso,
        on_delete=models.CASCADE,
		null = True,
		blank = True
    )
	
	def save(self, *args, **kwargs):
		slug_save(self) # call slug_save, listed below
		if(self.regression_type=="LI" and self.linear == None):
			linear = Linear()
			linear.save()
			self.linear = linear
		elif(self.regression_type=="LO" and self.logistic == None):
			logistic = Logistic()
			logistic.save()
			self.logistic = logistic
		elif(self.regression_type=="LA" and self.lasso == None):
			lasso = Lasso()
			lasso.save()
			self.lasso = lasso
		super(Project, self).save(*args, **kwargs)

	def delete(self, *args, **kwargs):
		if self.lasso != None:
			self.lasso.delete()
		if self.logistic != None:
			self.logistic.delete()
		if self.linear != None:
			self.linear.delete()
		return super(self.__class__, self).delete(*args, **kwargs)
		
	def get_absolute_url(self):
		return reverse("project-detail", kwargs={"pk": self.pk})
		

def slug_save(obj):
  if not obj.slug: # if there isn't a slug
    obj.slug = get_random_string(12) # create one
    slug_is_wrong = True  
    while slug_is_wrong: # keep checking until we have a valid slug
        slug_is_wrong = False
        other_objs_with_slug = type(obj).objects.filter(slug=obj.slug)
        if len(other_objs_with_slug) > 0:
            # if any other objects have current slug
            slug_is_wrong = True
        if slug_is_wrong:
            # create another slug and check it again
            obj.slug = get_random_string(12)		
