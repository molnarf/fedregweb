from django.db.models.signals import post_save
from django.dispatch import receiver

from .lasso_functions import lasso_init, lambda_init, lasso_update
from .models import Project, LambdaInit, LassoUpdate


# checks if the superuser started the Project and if, calls lasso_init
@receiver(post_save, sender=Project)
def lasso_init_handler(sender, instance, **kwargs):
    if instance.is_running != instance.old_running:
        if instance.is_running == True:
            if instance.regression_type == "LA":
                print("initializing lasso")
                lasso_init(instance.slug)

    instance.old_running = instance.is_running


# checks if all lambda init objects have been created and if, calls lambda_init
@receiver(post_save, sender=LambdaInit)
def lambda_init_handler(sender, instance, **kwargs):
    project = instance.lasso_project.project_set.first()
    lambdainits = project.lasso.lambdainit_set.all()
    if (len(lambdainits) == len(project.participants.all())):
        latest = True
        for init in lambdainits:
            if (instance.timestamp < init.timestamp):
                latest = False
        if (latest):
            print(project.slug + " lambda handeld")
            lambda_init(project.slug)


@receiver(post_save, sender=LassoUpdate)
def lasso_update_handler(sender, instance, **kwargs):
    project = instance.lasso_project.project_set.first()
    updates = project.lasso.lassoupdate_set.all().filter(iteration=instance.iteration)
    if (len(updates) == len(project.participants.all())):
        if (instance.pk == project.lasso.lassoupdate_set.last().pk):
            print(project.slug + " update handeld iteration: " + str(instance.iteration))
            lasso_update(project.slug, instance.iteration)
        else:
            print("not latest " + str(instance.iteration) + "pk: " +str(instance.pk))



