from django.apps import AppConfig


class WebinterfaceConfig(AppConfig):
    name = 'Webinterface'

    def ready(self):
        import Webinterface.signals


