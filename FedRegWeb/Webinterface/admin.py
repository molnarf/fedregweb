from django.contrib import admin
from .models import Project, Linear, Lasso, Logistic, LassoInit, Logistic_data, CovarianceMatrix, LambdaInit, LassoUpdate, Evaluate
#from Webinterface.models import Project

# Register your models here.



admin.site.register(Project)
admin.site.register(Linear)
admin.site.register(Lasso)
admin.site.register(Logistic)
admin.site.register(LassoInit)
admin.site.register(Logistic_data)
admin.site.register(CovarianceMatrix)
admin.site.register(LambdaInit)
admin.site.register(LassoUpdate)
admin.site.register(Evaluate) # logistic regression
